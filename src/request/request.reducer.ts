
import { 
  requestAPIActions,
} from './request.action';

import {
  createRequestActionType, updateResultFromRow, updateResultFromSearch,
} from '../utils';

import {
  RequestApiResult,
  RequestApiMeta,
  RequestAPIRespond,
} from '../types';

import { FluxStandardAction } from 'flux-standard-action';

/**
 * Function to create the reducer for each provided request.
 * 
 * @param {string} requestId The id for the created reducer
 * @returns {object} The redux state of the reducer
 */
export function createRequestReducer<ApiResult> (
  requestId: string,
) {

  const initialState: RequestApiResult<ApiResult> = {
    error: undefined,
    loaded: false,
    meta: undefined,
    result: undefined,
    updating: false,
  };

  /**
   * The actual redux reducer
   * @param {object} state
   * @param {object} 
   */
  function reducer (
    state: RequestApiResult<ApiResult> = initialState,
    action: FluxStandardAction<string, RequestApiResult<ApiResult>, RequestApiMeta>
  ): RequestApiResult<ApiResult> {
    const updateState = (action.meta && action.meta.update) || 
      (action.meta && action.meta.options && action.meta.options.update) ? 
        true : false;

    const isPager = action.meta && action.meta.options && action.meta.options.isPager ? true : false;
    const isSearch = action.meta && action.meta.options && action.meta.options.isSearch ? true : false;

    switch (action.type) {
      case createRequestActionType(requestAPIActions.LOAD_REQUEST, requestId):
        return {
          error: undefined,
          loaded: false,
          meta: {
            id: action.meta ? action.meta.id : '',
            options: action.payload ? action.payload : undefined,
            update: action.meta && action.meta.update,
            isPager: isPager,
            isSearch: isSearch,
          } as RequestApiMeta,
          result: updateState ? state.result : undefined,
          updating: true,
        };
      case createRequestActionType(requestAPIActions.LOAD_SUCCEEDED, requestId):
        return {
          error: undefined,
          loaded: true,
          meta: action.meta,
          result: action.payload as ApiResult | undefined,
          updating: false,
        };
      case createRequestActionType(requestAPIActions.LOAD_UPDATE, requestId):
        if (action.meta && action.meta.isSearch) {
          const resultFromSearch = updateResultFromSearch(
            state as RequestApiResult<{[key: string]: RequestAPIRespond<any>}>,
            action.payload && action.payload as any,
          )
          return {
            error: undefined,
            loaded: true,
            meta: action.meta,
            result: resultFromSearch as ApiResult | undefined,
            updating: false,
          };
        }
        const resultFromRow = updateResultFromRow<ApiResult>(
          state as RequestApiResult<RequestAPIRespond<any>>, 
          action.payload && action.payload as any
        );
        return {
          error: undefined,
          loaded: true,
          meta: action.meta,
          result: resultFromRow as ApiResult | undefined,
          updating: false,
        };
      case createRequestActionType(requestAPIActions.LOAD_FAILED, requestId):
        return {
          error: action.payload ? action.payload as any : undefined,
          loaded: false,
          meta: action.meta,
          result: (updateState && state.result) || undefined,
          updating: false,
        };
      case createRequestActionType(requestAPIActions.LOAD_PLAIN, requestId):
        return {
          error: undefined,
          loaded: true,
          meta: action.meta,
          result: action.payload as any,
          updating: false,
        };
      case createRequestActionType(requestAPIActions.LOAD_CANCELD, requestId):
        return state;
      case createRequestActionType(requestAPIActions.LOAD_CLEARED, requestId):
        return initialState;
      default:
        return state;
    }
  };

  return reducer;
}