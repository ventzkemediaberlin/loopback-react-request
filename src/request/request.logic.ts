
import { createLogic } from 'redux-logic';

import {  
  requestAPIActions,
} from './request.action';

import {
  HttpService,
} from '../service';

import {
  createRequestActionType, errorNotFound,
} from '../utils'

import {
  RequestApiMeta,
  RequestApiResult,
  AppSettings,
  AxiosErrorCustom,
  RequestApiHttpPayload,
} from '../types';

import { AxiosResponse } from 'axios';
import { FSA } from 'flux-standard-action';
import { unsuccessfulAuthenticated, clearAuthentification } from '../authenticator';

// type FSA = FluxStandardAction<{}, RequestApiHttpPayload>;
/**
 * Default redux logic that inits when the `requestAPIActions.load` action is dispached.
 * @param {string} requestId The Id that describes the current logic
 * @param {Object} appSettings Setting that will be passed to the HTTP Service
 */
export const createRequestLogic = <ApiResult>(requestId: string, appSettings: AppSettings, reducer: string[]) => {

  /**
   * 
   */
  const logic = createLogic({
    type: createRequestActionType(requestAPIActions.LOAD_REQUEST, requestId),
    cancelType: createRequestActionType(requestAPIActions.LOAD_CANCELD, requestId),
    latest: true,
    process: ( process, dispatcher, done) => {
      const action = process.action as FSA<string, RequestApiHttpPayload, RequestApiMeta> 
      const meta: RequestApiMeta = {
        id: requestId,
        options: action.payload,
      };
      const dispatch = dispatcher as any;
      return new HttpService<ApiResult>(action.payload as RequestApiHttpPayload, appSettings).fetch()
        .then((value: AxiosResponse<RequestApiResult<ApiResult>>) => {
          const {
            data,
            status,
            statusText,
          } = value;
          const result = data;
          meta.status = status;
          meta.statusText = statusText;
          meta.isSearch = meta.options && meta.options.isSearch;
          if (action.payload && meta && meta.options && meta.options.isPager) {
            meta.isPager = action.payload.isPager
            dispatch(requestAPIActions.loadUpdate<RequestApiResult<ApiResult>, RequestApiMeta>(requestId, result, meta));
          } else {
            dispatch(
              requestAPIActions.loadSucceeded<RequestApiResult<ApiResult>, RequestApiMeta>(requestId, result, meta)
            );
          }
        })
        .catch((error: AxiosErrorCustom) => {
          let data: RequestApiResult<ApiResult>['error'] = errorNotFound(error);
          meta.status = error.response && error.response.status || data && data.statusCode;
          meta.statusText = error.response && error.response.statusText || data && data.message;
          meta.update = action.meta && action.meta.update || false;
          if (meta.status === 401 && meta.statusText === 'Unauthorized') {
            dispatch(unsuccessfulAuthenticated());
            dispatch(clearAuthentification());
            reducer.map(state => dispatch(requestAPIActions.loadCleared(state)));
          }
          dispatch(
            requestAPIActions.loadFailed<RequestApiResult<ApiResult>['error'], RequestApiMeta>(requestId, data, meta));
        })
        .then(() => done());
    }
  }); 

  return logic;
};