import { createRequestActionType } from '../utils';
import { FSA } from 'flux-standard-action';
import { ClientInfo, RequestApiMeta, RequestApiError } from '../types';

export const LOADING_CLIENT_INFO = createRequestActionType('LOADING', 'client-info');
export const SUCCEEDED_CLIENT_INFO = createRequestActionType('SUCCEEDED', 'client-info');
export const FAILED_CLIENT_INFO = createRequestActionType('FAILED', 'client-info');
export const CANCEL_CLIENT_INFO = createRequestActionType('CANCEL', 'client-info');
export const CLEAR_CLIENT_INFO = createRequestActionType('CLEAR', 'client-info');

/**
 * Action to load client info
 * @returns FSA
 */
export const loadClientInfo = (): FSA<string> => ({
  type: LOADING_CLIENT_INFO
});

/**
 * Action to load client info
 * @param payload
 * @returns FSA
 */
export const succeededClientInfo = (payload: ClientInfo, meta: RequestApiMeta): FSA<string, ClientInfo, RequestApiMeta> => ({
  type: SUCCEEDED_CLIENT_INFO,
  payload,
  meta,
});

/**
 * Action to load client info
 * @param payload
 * @returns FSA
 */
export const failedClientInfo = (payload: RequestApiError, meta: RequestApiMeta): FSA<string, RequestApiError, RequestApiMeta> => ({
  type: FAILED_CLIENT_INFO,
  payload,
  meta,
});


/**
 * Action to load client info
 * @param payload
 * @returns FSA
 */
export const cancelClientInfo = (): FSA<string> => ({
  type: CANCEL_CLIENT_INFO,
});


/**
 * Action to load client info
 * @param payload
 * @returns FSA
 */
export const clearClientInfo = (): FSA<string> => ({
  type: CLEAR_CLIENT_INFO,
});


