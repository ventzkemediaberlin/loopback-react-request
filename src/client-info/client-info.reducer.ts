import { FSA } from 'flux-standard-action';

import { assign as _assign } from 'lodash';

import { 
  RequestApiResult, 
  ClientInfo, RequestApiError, 
  RequestApiMeta 
} from '../types';

import { 
  LOADING_CLIENT_INFO, SUCCEEDED_CLIENT_INFO, FAILED_CLIENT_INFO, CLEAR_CLIENT_INFO, CANCEL_CLIENT_INFO 
} from './client-info.action';

const initialState: RequestApiResult<ClientInfo> = {
  error: undefined,
  loaded: false,
  meta: undefined,
  result: undefined,
  updating: false,
};

export const clientInfoReducer = (state: RequestApiResult<ClientInfo> = initialState, action: FSA<string, ClientInfo | RequestApiError, RequestApiMeta>): RequestApiResult<ClientInfo> => {
  switch (action.type) {
    case LOADING_CLIENT_INFO: 
      return _assign({}, state, {
        updating: true,
        loaded: false,
      });
    case SUCCEEDED_CLIENT_INFO:
      return _assign({}, state, {
        error: undefined,
        loaded: true,
        result: action.payload,
        meta: action.meta,
        updating: false,
      });
    case FAILED_CLIENT_INFO:      
      return _assign({}, state, {
        error: action.payload,
        loaded: true,
        meta: action.meta,
        updating: false,
      });
    case CANCEL_CLIENT_INFO: 
      return _assign({}, state, {
        updating: true,
        loaded: false,
      });
    case CLEAR_CLIENT_INFO: 
      return initialState;
    default:
      return state;
  }
};