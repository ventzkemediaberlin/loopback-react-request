import { createLogic } from 'redux-logic';
import axios, { 
  AxiosRequestConfig, 
  AxiosPromise, 
  AxiosResponse, 
  AxiosError
} from 'axios';
import { assign as _assign } from 'lodash';
import { LOADING_CLIENT_INFO, CANCEL_CLIENT_INFO, succeededClientInfo, failedClientInfo } from './client-info.action';
import { RequestApiResult, ClientInfo, RequestApiMeta, RequestApiError } from '../types';;
import { errorNotFound } from '../utils';

type FetchStatic<AxiosResult> = (url: string, options?: AxiosRequestConfig) => AxiosPromise<AxiosResponse<AxiosResult>>;

const fetch: FetchStatic<ClientInfo> = axios;

export const clientInfoLogic = createLogic<RequestApiResult<ClientInfo>, ClientInfo, RequestApiMeta>({
  type: LOADING_CLIENT_INFO,
  cancelType: CANCEL_CLIENT_INFO,
  latest: true,
  process: ({ action }, dispatcher, done) => {
    const dispatch = dispatcher as any;
    fetch('https://ipapi.co/json/')
      .then((result: AxiosResponse) => {
        const data: ClientInfo = _assign<{}, ClientInfo, {timestamp: number}>({}, result.data, {
          timestamp: Math.floor(Date.now() / 1000),
        })
        dispatch(succeededClientInfo(data, {
          id: 'clientInfo',
          status: result.status,
          statusText: result.statusText,
        }));
      })
      .catch((data: AxiosError) => {
        const error = errorNotFound(data) as RequestApiError;
        dispatch(failedClientInfo(error as RequestApiError, {
          id: 'clientInfo'
        }));
      })
      .then(() => done());
  }
});