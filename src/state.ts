import { 
  assign as _assign,
  concat as _concat } from 'lodash';

import { 
  createRequestLogic, 
  createRequestReducer, 
} from './request';

import {
  CombineReducerWithLogic, 
  CreateRequestReducer, 
  AppSettings,
  ReducerIds
} from './types';

import { Logic } from 'redux-logic';
import { restApiConfig } from './config';
import { creatAuthenticationLogic, createAuthentificationReducer, createLogoutReducer, createValidateAccessTokenReducer, createAuthenticatorReducer } from './authenticator';
import { clientInfoLogic, clientInfoReducer } from './client-info';

const defaultAppSettings: AppSettings = {
  cookieName: 'authToken',
  config: restApiConfig,
}

/**
 * Class representing the the redux state and logic, 
 * it creates an array with all informations by a key
 * @class
 * @param {Array} identifiers Array of request Id's
 * @param {Object} appSettings Additional app settings.
 * @property {Object} appSettings Holds the additional app settings.
 * @property {Array} combinedLogic Holds all created redux logics.
 * @property {Object} combinedStates Holds all created redux states.
 * @property {Object} combinedReducerWithLogic Holds all redux logic, state and Id's.
 * @property {Array} identifiers Array of existing request Id's.
 */
export class RequestStore<ApiResult> {

  appSettings: AppSettings;

  get combinedLogic(): Logic<any>[] {
    const combinedLogic: Logic<any>[] = [];
    this.combinedReducerWithLogic.map(store => store.logic && combinedLogic.push(store.logic));
    return combinedLogic;
  }
  
  get combinedStates(): CreateRequestReducer<ApiResult> | {} {
    const combinedStates: CreateRequestReducer<ApiResult> | {} = {};
    this.combinedReducerWithLogic.map(store => store.reducer && _assign(combinedStates, {
      [store.id]: store.reducer,
    }));
    return combinedStates;
  }
  
  get combinedReducerWithLogic() {
    return this.combineReducerWithLogic<ApiResult>();
  }

  get defaultReducerWithLogic(): CombineReducerWithLogic<{}>[] {
    const authLogics = creatAuthenticationLogic(this.reducerIds, this.appSettings);
    return [{
      id: 'authenticator',
      reducer: createAuthenticatorReducer(this.appSettings),
    }, {
      id: 'authentification',
      logic: authLogics.fetchAuthentification(),
      reducer: createAuthentificationReducer(this.appSettings),
    }, {
      id: 'clientInfo',
      logic: clientInfoLogic,
      reducer: clientInfoReducer,
    } , {
      id: 'logout',
      logic: authLogics.fetchLogout(),
      reducer: createLogoutReducer(),
    }, {
      id: 'validateToken',
      logic: authLogics.validateAccessToken(),
      reducer: createValidateAccessTokenReducer(),
    }];
  }
  
  combineReducerWithLogic<R>(): CombineReducerWithLogic<R>[] {
    const combined: CombineReducerWithLogic<R>[] = [];
    this.identifiers.map(identifier => combined.push({
      id: identifier as string,
      logic: createRequestLogic<R>(identifier, this.appSettings, this.reducerIds.secure),
      reducer: createRequestReducer<R>(identifier),
    }));
    return _concat([], combined, this.defaultReducerWithLogic);
  }

  get identifiers(): string[] {
    return _concat([], this.reducerIds.public, this.reducerIds.secure);
  }
  
  reducerIds: ReducerIds = {
    public: [],
    secure: [],
  };

  constructor(identifiers: ReducerIds, appSettings?: AppSettings) {
    this.appSettings = appSettings ? appSettings : defaultAppSettings;
    this.reducerIds = identifiers;
  }
}
