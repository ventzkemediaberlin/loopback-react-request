import axios from 'axios';

import { 
  AxiosRequestConfig, 
  AxiosPromise, 
  AxiosResponse 
} from 'axios';

import { 
  assign as _assign,
} from 'lodash';

import {
  stringify
} from 'qs';

import { 
  AppSettings, 
  HttpServiceProps, 
} from './types';

import { getAccessTokenFromCookie } from './utils';

type FetchStatic<AxiosResult> = (url: string, config?: AxiosRequestConfig) => AxiosPromise<AxiosResponse<AxiosResult>>;

/**
 * Class representing the http service, it get called when dispatching the `requestAPIActions.load()` 
 * or `requestAPIActions.update()` action type.
 * 
 * ```javascript
 * var example = 'test
 * ```
 * 
 * @class HttpService
 * @param {Object} props Properties to set up the HttpService Class
 * @param {Object} appSettings Properties that are neccessary to get this package to run.
 * @property {Object} appSettings Object that holds the app settings.
 * @property {Object} props Object that holds the properties that are neccessary to get this package to run.
 * @property {Object} accessToken Object that holds the the Loopback access token.
 * @property {String} API_URL Url to the API.
 * @property {Object} headers Object that holds the http headers.
 * @property {Object} options Object that holds the AxiosRequestConfig
 * @property {String} queryParams Query parameter.
 * @property {String} queryType Describes the Query type, Default is `filter`
 * @property {String} url Url to Rest Api
 */
export class HttpService<ApiResult> {

  private appSettings: AppSettings;
  private props: HttpServiceProps;

  constructor(props: HttpServiceProps, appSettings: AppSettings) {
    this.appSettings = appSettings;
    this.props = props;
  }
  
  get accessToken() {
    return getAccessTokenFromCookie(this.appSettings.cookieName);
  }

  get API_URL(): string {
    const {config} = this.appSettings;
    const port = config.port ? `:${config.port}` : '';
    const path = config.path ? config.path : '';
    return `${config.protokol}://${config.url}${port}${path}`;
  }

  get headers() {
    return _assign({}, this.options.headers, {
      'Content-Type': 'application/json; charset=utf-8',
    });
  }

  get options(): AxiosRequestConfig {
    return _assign({}, this.props.options);
  }

  get queryParams(): string {
    if (this.queryType && this.queryType.length > 0) {
      return this.props.queryParams ? `?${stringify({[this.queryType]: this.props.queryParams})}` : '';
    } else {
      return this.props.queryParams ? `?${stringify(this.props.queryParams)}` : '';
    }
  }

  get queryType(): string | undefined {
    return this.props.queryType !== false ? this.props.queryType && this.props.queryType.length > 0 ? this.props.queryType  : 'filter' : undefined;
  }

  get url(): string {
    return `${this.API_URL}${this.props.url}${this.queryParams}`;
  }

  fetch = (): any => {
    const options = this.options;
    if (this.accessToken) {
      options.headers = _assign(this.options.headers, {
        'x-access-token': this.accessToken.id,
      });
    }
    const fetch: FetchStatic<ApiResult> = axios;
    return fetch(this.url, options);
  }
}