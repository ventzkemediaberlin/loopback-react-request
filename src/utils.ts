
import * as Cookie from 'js-cookie';
import { concat as _concat} from 'lodash';
import { AccessToken, RequestAPIRespond, RequestApiResult, AxiosErrorCustom } from './types';
import { AxiosError } from 'axios';
import { requestAPIActions } from './request';

/**
 * Function to create the the Redux actions type that will be dispatched by each state when 
 * calling an action.
 * @param {String} defaultType 
 * @param {String} extendedType 
 * @returns {String} Redux action type
 */
export function createRequestActionType(defaultType: string, extendedType: string ): string {
  return `@ReduxHttpRequest/${defaultType}_${extendedType.toUpperCase()}`;
}

/**
 * Get access token from cookie
 * @param {String} ACCESSTOKEN_NAME
 * @returns {Object} AccessToken
 */
export function getAccessTokenFromCookie(ACCESSTOKEN_NAME: string): AccessToken | undefined {
  const accessToken = Cookie.getJSON(ACCESSTOKEN_NAME) as AccessToken;
  if (accessToken && accessToken.id && accessToken.id.length) {
    return accessToken;
  }
  return undefined;
}

export function updateResultFromRow<M = {}>(
  state:  RequestApiResult<RequestAPIRespond<M>>,
  payload?: RequestAPIRespond<M>,
): RequestAPIRespond<M> | undefined {
  if (payload) {
    const prevResult = state && state.result as RequestAPIRespond<M>;
    if (prevResult) {
      const rows = _concat(prevResult.rows || [], payload.rows || []);
      return {
        start: prevResult.start,
        end: payload.end,
        rowCount: rows && rows.length,
        rows: rows && rows.length > 0 ? rows : [],
        totalRowCount: payload.totalRowCount,
      };
    } else {
      return payload;
    }
  }
  return;
}

export function updateResultFromSearch<M = {}>(
  state:  RequestApiResult<{[key: string]: RequestAPIRespond<M>}>,
  payload?: {[key: string]: RequestAPIRespond<M>},
): {[key: string]: RequestAPIRespond<M>} | undefined  {
  if (payload) {
    const prevResult = state && state.result as {[key: string]: RequestAPIRespond<M>};
    if (prevResult) {
      const keys = Object.keys(prevResult);
      const newResult = {};
      keys.map(key => {
        const resultFromState = prevResult[key];
        const resultFromPayload = payload[key];      
        if (resultFromState) {
          const rows = _concat(resultFromState.rows || [], resultFromPayload.rows || []);
          newResult[key] = {
            start: resultFromState.start,
            end: resultFromPayload.end,
            rowCount: rows && rows.length,
            rows: rows && rows.length > 0 ? rows : [],
            totalRowCount: resultFromPayload.totalRowCount,
          }
        } else {
          newResult[key] = resultFromPayload;
        }        
      });
      return newResult;
    } else {
      return payload;
    }
  }
  return;
}

export const errorNotFound = <R = {}>(error: AxiosError | AxiosErrorCustom): RequestApiResult<R>['error'] => {
  const responseError = 
    error.response && error.response.data && error.response.data.error ?
      error.response.data.error :
      error.response && error.response.data;
  
  return responseError || {
    code: 'NOT_FOUND',
    message: 'Could not connect to Api',
    name: 'Not found',
    statusCode: 404,
  };
};

export interface ClearStateProps<M>{
  dispatch: Function;
  key?: string;
  state?: RequestApiResult<M>[];
}

/**
 * Clears a list of states stored in global state by given keys.
 * @param {Array} keys List of keys
 * @param {ClearStateProps} props Object that holds all information for clearing states
 * @returns Function
 */
export const clearStateByListOfKeys = <M>(
  keys: string[], props: ClearStateProps<M>
) => keys.forEach(
  key => clearRequestStates({
    dispatch: props.dispatch,
    key,
    state: props.state && props.state[key] ? props.state[key] : undefined,
  })
);

/**
 * Clears one state bay given key
 * @param {ClearStateProps} props Object that holds all information for clearing states
 */
export const clearRequestStates = <M>(
  props: ClearStateProps<M>
): void => 
  props.state && props.key && props.state[props.key] && (props.state[props.key].result as M) && 
  props.key && props.dispatch(requestAPIActions.loadCleared(props.key));