import { ConfigRestApi } from './types';

const headers = {};

export const restApiConfig: ConfigRestApi = {
  port: 3300,
  protokol: 'http',
  url: 'localhost',
  path: '/api'
};

export default {
  headers,
  restApiConfig,
};