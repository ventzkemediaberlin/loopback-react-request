import * as Cookie from 'js-cookie';
import { createLogic } from 'redux-logic';
import { AxiosResponse, AxiosError } from 'axios';

import {
  failedAuthentification,
  succeededAuthentification,
  successfulAuthenticated,
  succeededLogout,
  failedLogout,
  unsuccessfulAuthenticated,
  clearAuthentification,
  clearLogout,
  failedAccessTokenValidation, 
  clearAccessTokenValidation,
  CANCEL_LOGIN,
  LOADING_LOGIN,
  LOADING_LOGOUT,
  CANCEL_LOGOUT,
  LOADING_VALIDATION, 
  CANCEL_VALIDATION,
  succeededAccessTokenValidation, 
} from './authenticator.action';

import { 
  AccessToken, 
  AxiosErrorCustom, 
  RequestApiError,
  AppSettings,
  ReducerIds,
} from '../types';

import {
  errorNotFound
} from '../utils';

import { HttpService } from '../service';

import { requestAPIActions } from '../request/request.action';

/**
 * Save access token to cookie
 * @param accessToken 
 * @returns void;
 */
export function saveAccessTokenToCookie(accessToken: AccessToken, name: string): void {
  const ttlInHours = accessToken.ttl / 60 / 60;
  const expires = ttlInHours / 24;  
  Cookie.set(name, accessToken, { expires: expires, path: '/' });
}

/**
 * Remove access token cookie
 */
export function removeAccessTokenFromCookie(name: string): void {
  Cookie.remove(name, { path: '/', });
}

const setUrlToUserRemote = (path?: string): string => {
  return path && path.length ? path : '/Users';
};

export const creatAuthenticationLogic = (reducer: ReducerIds, appSettings: AppSettings) => ({

  /**
   * Redux-logic to fetch user login
   */
  fetchAuthentification: () => {
    const logic = createLogic<any, Object>({
      type: LOADING_LOGIN,
      cancelType: CANCEL_LOGIN,
      latest: true,
      process: ({ action }, dispatcher, done) => {
        const dispatch = dispatcher as any;
        return new HttpService({
          options: {
            method: 'POST',
            data: action.payload,
          },
          url: `${setUrlToUserRemote(appSettings.userRemote)}/login`,
        }, appSettings)
          .fetch()
          .then((res: AxiosResponse) => {
            reducer.public.map(
              (state: string) => 
                dispatch(requestAPIActions.loadCleared(state))
            );
            return res;
          })
          .then((res: AxiosResponse) => {
            saveAccessTokenToCookie(res.data, appSettings.cookieName);
            return res;
          })
          .then((res: AxiosResponse) => {
            dispatch(successfulAuthenticated(res.data));
            return res;
          })
          .then((res: AxiosResponse) => {
            dispatch(succeededAuthentification(res.data));
            return res;
          })
          .then(() => dispatch(clearAccessTokenValidation()))
          .catch((data: AxiosErrorCustom) => {
            const error = errorNotFound(data);
            dispatch(failedAuthentification(error as RequestApiError));
          })
          .then(() => done());
      }
    });
    return logic;
  },

  /**
   * Redux logic to fetch logout
   */
  fetchLogout: () => {
    const logic = createLogic<any, AccessToken>({
      type: LOADING_LOGOUT,
      cancelType: CANCEL_LOGOUT,
      latest: true,
      process: ({ action }, dispatcher, done) => {
        const dispatch = dispatcher as any;
        const accessToken = action.payload as AccessToken;
        return new HttpService({
          options: {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json; charset=utf-8',
              'x-access-token': accessToken.id,
            },
          },
          url: `${setUrlToUserRemote(appSettings.userRemote)}/logout`,
        }, appSettings)
          .fetch()
          .then((res: AxiosResponse) => {
            reducer.secure.map(
              (state: string) => 
                dispatch(requestAPIActions.loadCleared(state))
            );
            return res;
          })
          .then((res: AxiosResponse) => {
            removeAccessTokenFromCookie(appSettings.cookieName);
            return res;
          })
          .then((res: AxiosResponse) => {
            dispatch(unsuccessfulAuthenticated());
            return res;
          })
          .then((res: AxiosResponse) => {
            dispatch(clearAuthentification());
            return res;
          })
          .then((res: AxiosResponse) => {
            dispatch(clearAccessTokenValidation());
            return res;
          })
          .then(() => dispatch(succeededLogout()))
          .then(() => dispatch(clearLogout()))
          .catch((error: AxiosError) => {
            const data = errorNotFound(error);
            return dispatch(failedLogout(data as RequestApiError));
          })
          .then(() => done());
      }
    });
    return logic;
  },

  /**
   * Redux logic to validate an existing token,
   */

  validateAccessToken: () => {
    const logic = createLogic<any, AccessToken>({
      type: LOADING_VALIDATION,
      cancelType: CANCEL_VALIDATION,
      latest: true,
      process: ({ action }, dispatcher, done) => {
        const dispatch = dispatcher as any;
        const accessToken = action.payload as AccessToken;
        return new HttpService({
          options: {
            headers: {
              'Content-Type': 'application/json; charset=utf-8',
              'x-access-token': accessToken.id,
            },
          },
          url: `${setUrlToUserRemote(appSettings.userRemote)}/${accessToken.userId}/accessTokens/${accessToken.id}`,
        }, appSettings)
          .fetch()
          .then((res: AxiosResponse) => {
            dispatch(succeededAccessTokenValidation(res.data));
            return res;
          })
          .then((res: AxiosResponse) => {
            return dispatch(succeededAuthentification(res.data));
          }).catch((error: AxiosError) => {
            const data = errorNotFound(error);
            return dispatch(failedAccessTokenValidation(data as RequestApiError));
          })
          .then(() => done());
      },
    });
    return logic;
  }
});
