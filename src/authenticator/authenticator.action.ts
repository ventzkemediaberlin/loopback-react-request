import {
  AuthentificationData,
  AuthentificationAction,
  AccessToken,
  RequestApiError,
  LogoutAction,
} from '../types';

import { createRequestActionType } from '../utils';

export const LOADING_VALIDATION = 'LOADING_VALIDATION';
export const SUCCESSFUL_VALIDATION = 'SUCCESSFUL_VALIDATION';
export const FAILED_LOGIN = 'FAILED_LOGIN';
export const CANCEL_VALIDATION = 'CANCEL_VALIDATION';
export const CLEAR_VALIDATION = 'CLEAR_VALIDATION';

export const SUCCESSFUL_AUTHENTICATED = 'SUCCESSFUL_AUTHENTICATED';
export const UNSUCCESSFUL_AUTHENTICATED = 'UNSUCCESSFUL_AUTHENTICATED';

export const LOADING_LOGIN = createRequestActionType('LOADING', 'login');
export const SUCCEEDED_LOGIN = createRequestActionType('SUCCEEDED', 'login');
export const FAILED_VALIDATION = createRequestActionType('FAILED', 'login');
export const CANCEL_LOGIN = createRequestActionType('CANCEL', 'login');
export const CLEAR_LOGIN = createRequestActionType('CLEAR', 'login');

export const LOADING_LOGOUT = createRequestActionType('LOADING', 'logout');
export const SUCCEEDED_LOGOUT = createRequestActionType('SUCCEEDED', 'logout');
export const FAILED_LOGOUT = createRequestActionType('FAILED', 'logout');
export const CANCEL_LOGOUT = createRequestActionType('CANCEL', 'logout');
export const CLEAR_LOGOUT = createRequestActionType('CLEAR', 'logout');

export type CANCEL_LOGIN_TYPE = 'CANCEL_LOGIN';
export type CANCEL_VALIDATION = 'CANCEL_VALIDATION';
export type CLEAR_LOGIN_TYPE = 'CLEAR_LOGIN';
export type CLEAR_LOGOUT_TYPE = 'CLEAR_LOGOUT';
export type CLEAR_VALIDATION = 'CLEAR_VALIDATION';
export type CANCEL_LOGOUT_TYPE = 'CANCEL_LOGOUT';
export type FAILED_LOGIN_TYPE = 'FAILED_LOGIN';
export type FAILED_LOGOUT_TYPE = 'FAILED_LOGOUT';
export type FAILED_VALIDATION = 'FAILED_VALIDATION';
export type LOADING_LOGIN_TYPE = 'LOADING_LOGIN';
export type LOADING_LOGOUT_TYPE = 'LOADING_LOGOUT';
export type LOADING_VALIDATION = 'LOADING_VALIDATION';
export type SUCCESSFUL_AUTHENTICATED_TYPE = 'SUCCESSFUL_AUTHENTICATED';
export type SUCCESSFUL_VALIDATION = 'SUCCESSFUL_VALIDATION';
export type SUCCEEDED_LOGIN_TYPE = 'SUCCEEDED_LOGIN';
export type SUCCEEDED_LOGOUT_TYPE = 'SUCCEEDED_LOGOUT';
export type UNSUCCESSFUL_AUTHENTICATED_TYPE = 'UNSUCCESSFUL_AUTHENTICATED';

export interface ValidateAccessTokenReturn {
  type: LOADING_VALIDATION | SUCCESSFUL_VALIDATION;
  payload: AccessToken;
}

export interface AuthenticatorAction {
  type: SUCCESSFUL_AUTHENTICATED_TYPE | UNSUCCESSFUL_AUTHENTICATED_TYPE;
  payload?: AccessToken;
}

/**
 * @param void
 * @returns AuthenticatorUnsuccessfulAction
 */
export function unsuccessfulAuthenticated(): AuthenticatorAction {
  return {
    type: UNSUCCESSFUL_AUTHENTICATED
  };
}

/**
 * @param void
 * @returns AuthenticatorAction
 */
export function successfulAuthenticated(payload: AccessToken): AuthenticatorAction {
  return {
    type: SUCCESSFUL_AUTHENTICATED,
    payload: payload,
  };
}

/**
 * Action to load login request
 * @param payload 
 * @returns AuthentificationAction
 */
export function loadAuthentification(payload: AuthentificationData): AuthentificationAction {
  return {
    type: LOADING_LOGIN,
    payload: payload,
  };
}

/**
 * Action to dispatch succsseful Login 
 * @param payload
 * @returns AuthentificationAction
 */
export function succeededAuthentification(payload: AccessToken): AuthentificationAction {
  return {
    type: SUCCEEDED_LOGIN,
    payload: payload,
  };
}

/**
 * 
 * @param payload 
 * @returns AuthentificationAction
 */
export function failedAuthentification(payload: RequestApiError): AuthentificationAction {
  return {
    type: FAILED_LOGIN,
    payload: payload,
  };
}

export function clearAuthentification(): AuthentificationAction {
  return {
    type: CLEAR_LOGIN,
    payload: undefined,
  };
}

export function loadLogout(payload?: AccessToken): LogoutAction {
  return {
    type: LOADING_LOGOUT,
    payload: payload,
  };
}

export function succeededLogout(): LogoutAction {
  return {
    type: SUCCEEDED_LOGOUT,
  };
}

export function failedLogout(payload?: RequestApiError): LogoutAction {
  return {
    type: FAILED_LOGOUT,
    payload: payload,
  };
}

export function clearLogout(): LogoutAction {
  return {
    type: CLEAR_LOGOUT,
  };
}

/**
 * 
 * @param {AccessToken} accessToken 
 * @returns {Object}
 */
export function loadAccessTokenValidation(accessToken: AccessToken): ValidateAccessTokenReturn {
  return {
    type: LOADING_VALIDATION,
    payload: accessToken,
  };
}

/**
 * 
 * @param {AccessToken} accessToken 
 * @returns {Object}
 */
export function succeededAccessTokenValidation(accessToken: AccessToken): ValidateAccessTokenReturn {
  return {
    type: SUCCESSFUL_VALIDATION,
    payload: accessToken,
  };
}

/**
 * 
 * @returns {Object}
 */
export function failedAccessTokenValidation(error: RequestApiError): {
  type: string;
  payload: RequestApiError;
} {
  return {
    type: FAILED_VALIDATION,
    payload: error,
  };
} 

export function clearAccessTokenValidation(): {
  type: string;
} {
  return {
    type: CLEAR_VALIDATION,
  };
}