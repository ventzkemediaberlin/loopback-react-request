import { assign as _assign } from 'lodash';

import { getAccessTokenFromCookie } from '../utils';

import {
  AuthentificationAction,
  AuthentificationState,
  IsAuthentificatedState,
  LogoutAction,
  LogoutState,
  RequestApiError,
  AccessToken,
  AppSettings,
  ValidateTokenState
} from '../types';

import {
  AuthenticatorAction,
  LOADING_VALIDATION, 
  SUCCESSFUL_VALIDATION, 
  CANCEL_VALIDATION, 
  FAILED_VALIDATION, 
  CLEAR_VALIDATION, 
  SUCCEEDED_LOGOUT,
  LOADING_LOGOUT,
  CANCEL_LOGOUT,
  FAILED_LOGOUT,
  CLEAR_LOGIN,
  CLEAR_LOGOUT,
  LOADING_LOGIN,
  SUCCESSFUL_AUTHENTICATED,
  UNSUCCESSFUL_AUTHENTICATED,
  SUCCEEDED_LOGIN,
  CANCEL_LOGIN,
  FAILED_LOGIN,
} from './authenticator.action';

function isAuthentificatedState(name: string): IsAuthentificatedState {
  const token = getAccessTokenFromCookie(name);
  return {
    accessToken: token,
    isAuthentificated: token && token.id.length ? true : false,
  };
}

export const createAuthenticatorReducer = (appSettings: AppSettings) => (
  state: IsAuthentificatedState = isAuthentificatedState(appSettings.cookieName),
  action: AuthenticatorAction,
): IsAuthentificatedState => {
  switch (action.type) {
    case SUCCESSFUL_AUTHENTICATED:
      return _assign({}, {
        accessToken: action.payload,
        isAuthentificated: true,
      });
    case UNSUCCESSFUL_AUTHENTICATED:
      return _assign({}, {
        accessToken: undefined,
        isAuthentificated: false,
      });
    default:
      return state;
  }
}

function authentificationState(name: string): AuthentificationState {
  const token = getAccessTokenFromCookie(name);
  return {
    accessToken: token,
    error: undefined,
    loaded: false,
    updating: false,
  };
}

/**
 * 
 * @param state 
 * @param action 
 * @returns AuthentificationState
 */
export const createAuthentificationReducer = (appSettings: AppSettings) => (
  state: AuthentificationState = authentificationState(appSettings.cookieName),
  action: AuthentificationAction,
): AuthentificationState => {
  switch (action.type) {
    case LOADING_LOGIN:
      return _assign({}, {
        accessToken: undefined,
        error: undefined,
        loaded: false,
        updating: true,
      });
    case SUCCEEDED_LOGIN:
      return _assign({}, {
        accessToken: action.payload as AccessToken,
        error: undefined,
        updating: false,
        loaded: true,
      });
    case FAILED_LOGIN:
      return _assign({}, {
        accessToken: undefined,
        error: action.payload as RequestApiError,
        updating: false,
        loaded: true,
      });
    case CANCEL_LOGIN:
      return state;
    case CLEAR_LOGIN:
      return _assign({}, {
        accessToken: undefined,
        error: undefined,
        updating: false,
        loaded: false,
      });
    default:
      return state;
  }
};

const logoutState: LogoutState = {
  error: undefined,
  loaded: false,
  updating: false,
};

export const createLogoutReducer = () => (
  state: LogoutState = logoutState,
  action: LogoutAction,
): LogoutState => {
  switch (action.type) {
    case LOADING_LOGOUT:
      return _assign({}, {
        error: undefined,
        loaded: false,
        updating: true
      });
    case SUCCEEDED_LOGOUT:
      return _assign({}, {
        error: undefined,
        loaded: true,
        updating: false
      });
    case FAILED_LOGOUT:
      return _assign({}, {
        error: action.payload as RequestApiError,
        loaded: false,
        updating: false
      });
    case CLEAR_LOGOUT:
      return _assign({}, {
        error: undefined,
        loaded: false,
        updating: false
      });
    case CANCEL_LOGOUT:
      return state;
    default:
      return state;
  }
}

export const accessTokenState: ValidateTokenState = {
  accessToken: undefined,
  error: undefined,
  loaded: false,
  ttlLeft: 0,
  updating: false,
};

const calculateLeftTtl = (crDate: Date, ttl: number): number => {
  const dateNowInSeconds = new Date().getTime() / 1000;
  const dateCreatedInSeconds = new Date(crDate).getTime() / 1000;
  const lifeTime = dateNowInSeconds - dateCreatedInSeconds;
  return Math.floor(ttl - lifeTime);
};

export const createValidateAccessTokenReducer = () => (
  state: ValidateTokenState = accessTokenState, 
  action: {
    type: string;
    payload?: AccessToken | RequestApiError;
  }
): ValidateTokenState => {
  switch (action.type) {
    case LOADING_VALIDATION:
      return {
        accessToken: undefined,
        error: undefined,
        loaded: false,
        ttlLeft: 0,
        updating: true,
      };
    case SUCCESSFUL_VALIDATION: 
      const accessToken: AccessToken | undefined = action.payload as AccessToken;
      return accessToken ? {
        accessToken,
        error: undefined,
        loaded: true,
        ttlLeft: calculateLeftTtl(accessToken.created, accessToken.ttl),
        updating: false,
      } : accessTokenState;
    case FAILED_VALIDATION:
      return {
        accessToken: undefined,
        error: action.payload as RequestApiError,
        loaded: true,
        ttlLeft: 0,
        updating: false,
      };
    case CLEAR_VALIDATION:
      return accessTokenState;
    case CANCEL_VALIDATION: 
      return state;
    default: 
      return state;
  }
}
