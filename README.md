<!-- DO NOT EDIT README.md (It will be overridden by README.hbs) -->

# Create React-Redux states for requests to Loopback API's

With this package you can easily create React-Redux states that holds data of an 
Ajax request.

Handling data from an API can often be messy, you will need to write an own 
Redux-Action, Redux-Reducer and somtimes special Logic for each Url you like 
to fetch.

To simplify this process we developed this tool to automate everything. By adding 
an identifier to the main Class in `global.store.js` an own instance including Redux-actions, 
-Reducers and -Logics will be created and added to the global state of your 
Application.

## Usage example with Redux and Redux-Logic

store.js

```javascript
const myStore = new RequestStore(['fetchData'], {
  config: {
    port: 3300,
    protokol: 'http',
    url: 'domain.tld',
    path: '/api'
  },
});

const logicMiddleware = createLogicMiddleware(myStore.combinedLogic);

const middleware = applyMiddleware(
  logicMiddleware,
);

const reducer = Object.assign(
  {}, 
  myStore.combinedStates, 
);

const combinedReducers = combineReducers(reducer);

export const store = () => {
  return createStore(
    combinedReducers,
    middleware, 
  );
};
```

App.js
```jsx
class App extends React.Component {
  componentDidMount() {
    this.props.dispatch(requestAPIActions.load(
      'fetchData', {
        url: '/urt-to-data',
      },
    ));
  }
  public render() {
    return (
      <div>
        {this.props.fetchData && this.props.fetchData.map(data, index => 
          <ListData key={index} data={data} />
        )}
      </div>
    );
  }
}

export const = connect(state => ({
  fetchData = state.fetchData,
}))(App);

```

# Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


  - [Setup](#setup)
- [React integration](#react-integration)
  - [Install dependencies](#install-dependencies)
- [API](#api)
  - [Classes](#classes)
  - [Members](#members)
  - [Functions](#functions)
  - [HttpService](#httpservice)
    - [new HttpService(props, appSettings)](#new-httpserviceprops-appsettings)
  - [RequestStore](#requeststore)
    - [new RequestStore(identifiers, appSettings)](#new-requeststoreidentifiers-appsettings)
  - [requestAPIActions](#requestapiactions)
    - [requestAPIActions.load(requestId, payload) ⇒ <code>Object</code>](#requestapiactionsloadrequestid-payload-%E2%87%92-codeobjectcode)
    - [requestAPIActions.loadSucceeded(requestId, payload, meta) ⇒ <code>Object</code>](#requestapiactionsloadsucceededrequestid-payload-meta-%E2%87%92-codeobjectcode)
    - [requestAPIActions.loadFailed(requestId, payload, meta) ⇒ <code>Object</code>](#requestapiactionsloadfailedrequestid-payload-meta-%E2%87%92-codeobjectcode)
    - [requestAPIActions.loadCanceld(requestId, payload) ⇒ <code>Object</code>](#requestapiactionsloadcanceldrequestid-payload-%E2%87%92-codeobjectcode)
    - [requestAPIActions.loadCleared(requestId) ⇒ <code>Object</code>](#requestapiactionsloadclearedrequestid-%E2%87%92-codeobjectcode)
    - [requestAPIActions.loadPlain(requestId, payload) ⇒ <code>Object</code>](#requestapiactionsloadplainrequestid-payload-%E2%87%92-codeobjectcode)
  - [unsuccessfulAuthenticated(void) ⇒](#unsuccessfulauthenticatedvoid-%E2%87%92)
  - [successfulAuthenticated(void) ⇒](#successfulauthenticatedvoid-%E2%87%92)
  - [loadAuthentification(payload) ⇒](#loadauthentificationpayload-%E2%87%92)
  - [succeededAuthentification(payload) ⇒](#succeededauthentificationpayload-%E2%87%92)
  - [failedAuthentification(payload) ⇒](#failedauthentificationpayload-%E2%87%92)
  - [loadAccessTokenValidation(accessToken) ⇒ <code>Object</code>](#loadaccesstokenvalidationaccesstoken-%E2%87%92-codeobjectcode)
  - [succeededAccessTokenValidation(accessToken) ⇒ <code>Object</code>](#succeededaccesstokenvalidationaccesstoken-%E2%87%92-codeobjectcode)
  - [failedAccessTokenValidation() ⇒ <code>Object</code>](#failedaccesstokenvalidation-%E2%87%92-codeobjectcode)
  - [saveAccessTokenToCookie(accessToken) ⇒](#saveaccesstokentocookieaccesstoken-%E2%87%92)
  - [removeAccessTokenFromCookie()](#removeaccesstokenfromcookie)
  - [fetchAuthentification()](#fetchauthentification)
  - [fetchLogout()](#fetchlogout)
  - [validateAccessToken()](#validateaccesstoken)
  - [createAuthentificationReducer(state, action) ⇒](#createauthentificationreducerstate-action-%E2%87%92)
  - [loadClientInfo() ⇒](#loadclientinfo-%E2%87%92)
  - [succeededClientInfo(payload) ⇒](#succeededclientinfopayload-%E2%87%92)
  - [failedClientInfo(payload) ⇒](#failedclientinfopayload-%E2%87%92)
  - [cancelClientInfo(payload) ⇒](#cancelclientinfopayload-%E2%87%92)
  - [clearClientInfo(payload) ⇒](#clearclientinfopayload-%E2%87%92)
  - [createRequestLogic(requestId, appSettings)](#createrequestlogicrequestid-appsettings)
    - [createRequestLogic~logic](#createrequestlogiclogic)
  - [createRequestReducer(requestId) ⇒ <code>object</code>](#createrequestreducerrequestid-%E2%87%92-codeobjectcode)
    - [createRequestReducer~reducer(state, action)](#createrequestreducerreducerstate-action)
  - [createRequestActionType(defaultType, extendedType) ⇒ <code>String</code>](#createrequestactiontypedefaulttype-extendedtype-%E2%87%92-codestringcode)
  - [getAccessTokenFromCookie(ACCESSTOKEN_NAME) ⇒ <code>Object</code>](#getaccesstokenfromcookieaccesstoken_name-%E2%87%92-codeobjectcode)
  - [clearStateByListOfKeys(keys, props) ⇒](#clearstatebylistofkeyskeys-props-%E2%87%92)
  - [clearRequestStates(props)](#clearrequeststatesprops)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Setup

```
npm install --save https://ventzke-media@bitbucket.org/ventzkemediaberlin/loopback-react-request.git
or
yarn add https://ventzke-media@bitbucket.org/ventzkemediaberlin/loopback-react-request.git
```



# React integration

## Install dependencies


Code samples

# API
## Classes

<dl>
<dt><a href="#HttpService">HttpService</a></dt>
<dd></dd>
<dt><a href="#RequestStore">RequestStore</a></dt>
<dd></dd>
</dl>

## Members

<dl>
<dt><a href="#requestAPIActions">requestAPIActions</a></dt>
<dd><p>This is an Object that holds the Redux action types and action functions to dispatch
actions from React or Redux logic to act with the redux state.</p></dd>
</dl>

## Functions

<dl>
<dt><a href="#unsuccessfulAuthenticated">unsuccessfulAuthenticated(void)</a> ⇒</dt>
<dd></dd>
<dt><a href="#successfulAuthenticated">successfulAuthenticated(void)</a> ⇒</dt>
<dd></dd>
<dt><a href="#loadAuthentification">loadAuthentification(payload)</a> ⇒</dt>
<dd><p>Action to load login request</p></dd>
<dt><a href="#succeededAuthentification">succeededAuthentification(payload)</a> ⇒</dt>
<dd><p>Action to dispatch succsseful Login</p></dd>
<dt><a href="#failedAuthentification">failedAuthentification(payload)</a> ⇒</dt>
<dd></dd>
<dt><a href="#loadAccessTokenValidation">loadAccessTokenValidation(accessToken)</a> ⇒ <code>Object</code></dt>
<dd></dd>
<dt><a href="#succeededAccessTokenValidation">succeededAccessTokenValidation(accessToken)</a> ⇒ <code>Object</code></dt>
<dd></dd>
<dt><a href="#failedAccessTokenValidation">failedAccessTokenValidation()</a> ⇒ <code>Object</code></dt>
<dd></dd>
<dt><a href="#saveAccessTokenToCookie">saveAccessTokenToCookie(accessToken)</a> ⇒</dt>
<dd><p>Save access token to cookie</p></dd>
<dt><a href="#removeAccessTokenFromCookie">removeAccessTokenFromCookie()</a></dt>
<dd><p>Remove access token cookie</p></dd>
<dt><a href="#fetchAuthentification">fetchAuthentification()</a></dt>
<dd><p>Redux-logic to fetch user login</p></dd>
<dt><a href="#fetchLogout">fetchLogout()</a></dt>
<dd><p>Redux logic to fetch logout</p></dd>
<dt><a href="#validateAccessToken">validateAccessToken()</a></dt>
<dd><p>Redux logic to validate an existing token,</p></dd>
<dt><a href="#createAuthentificationReducer">createAuthentificationReducer(state, action)</a> ⇒</dt>
<dd></dd>
<dt><a href="#loadClientInfo">loadClientInfo()</a> ⇒</dt>
<dd><p>Action to load client info</p></dd>
<dt><a href="#succeededClientInfo">succeededClientInfo(payload)</a> ⇒</dt>
<dd><p>Action to load client info</p></dd>
<dt><a href="#failedClientInfo">failedClientInfo(payload)</a> ⇒</dt>
<dd><p>Action to load client info</p></dd>
<dt><a href="#cancelClientInfo">cancelClientInfo(payload)</a> ⇒</dt>
<dd><p>Action to load client info</p></dd>
<dt><a href="#clearClientInfo">clearClientInfo(payload)</a> ⇒</dt>
<dd><p>Action to load client info</p></dd>
<dt><a href="#createRequestLogic">createRequestLogic(requestId, appSettings)</a></dt>
<dd><p>Default redux logic that inits when the <code>requestAPIActions.load</code> action is dispached.</p></dd>
<dt><a href="#createRequestReducer">createRequestReducer(requestId)</a> ⇒ <code>object</code></dt>
<dd><p>Function to create the reducer for each provided request.</p></dd>
<dt><a href="#createRequestActionType">createRequestActionType(defaultType, extendedType)</a> ⇒ <code>String</code></dt>
<dd><p>Function to create the the Redux actions type that will be dispatched by each state when
calling an action.</p></dd>
<dt><a href="#getAccessTokenFromCookie">getAccessTokenFromCookie(ACCESSTOKEN_NAME)</a> ⇒ <code>Object</code></dt>
<dd><p>Get access token from cookie</p></dd>
<dt><a href="#clearStateByListOfKeys">clearStateByListOfKeys(keys, props)</a> ⇒</dt>
<dd><p>Clears a list of states stored in global state by given keys.</p></dd>
<dt><a href="#clearRequestStates">clearRequestStates(props)</a></dt>
<dd><p>Clears one state bay given key</p></dd>
</dl>

<a name="HttpService"></a>

## HttpService
**Kind**: global class  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| appSettings | <code>Object</code> | <p>Object that holds the app settings.</p> |
| props | <code>Object</code> | <p>Object that holds the properties that are neccessary to get this package to run.</p> |
| accessToken | <code>Object</code> | <p>Object that holds the the Loopback access token.</p> |
| API_URL | <code>String</code> | <p>Url to the API.</p> |
| headers | <code>Object</code> | <p>Object that holds the http headers.</p> |
| options | <code>Object</code> | <p>Object that holds the AxiosRequestConfig</p> |
| queryParams | <code>String</code> | <p>Query parameter.</p> |
| queryType | <code>String</code> | <p>Describes the Query type, Default is <code>filter</code></p> |
| url | <code>String</code> | <p>Url to Rest Api</p> |

<a name="new_HttpService_new"></a>

### new HttpService(props, appSettings)
<p>Class representing the http service, it get called when dispatching the <code>requestAPIActions.load()</code>
or <code>requestAPIActions.update()</code> action type.</p>
<pre class="prettyprint source lang-javascript"><code>var example = 'test
</code></pre>


| Param | Type | Description |
| --- | --- | --- |
| props | <code>Object</code> | <p>Properties to set up the HttpService Class</p> |
| appSettings | <code>Object</code> | <p>Properties that are neccessary to get this package to run.</p> |

<a name="RequestStore"></a>

## RequestStore
**Kind**: global class  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| appSettings | <code>Object</code> | <p>Holds the additional app settings.</p> |
| combinedLogic | <code>Array</code> | <p>Holds all created redux logics.</p> |
| combinedStates | <code>Object</code> | <p>Holds all created redux states.</p> |
| combinedReducerWithLogic | <code>Object</code> | <p>Holds all redux logic, state and Id's.</p> |
| identifiers | <code>Array</code> | <p>Array of existing request Id's.</p> |

<a name="new_RequestStore_new"></a>

### new RequestStore(identifiers, appSettings)
<p>Class representing the the redux state and logic,
it creates an array with all informations by a key</p>


| Param | Type | Description |
| --- | --- | --- |
| identifiers | <code>Array</code> | <p>Array of request Id's</p> |
| appSettings | <code>Object</code> | <p>Additional app settings.</p> |

<a name="requestAPIActions"></a>

## requestAPIActions
<p>This is an Object that holds the Redux action types and action functions to dispatch
actions from React or Redux logic to act with the redux state.</p>

**Kind**: global variable  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| LOAD_CANCELD | <code>String</code> | <p>Describes the action when an request is canceld.</p> |
| LOAD_CLEARED | <code>String</code> | <p>Describes the action to clear the state.</p> |
| LOAD_FAILED | <code>String</code> | <p>Describes the action when an request has failed.</p> |
| LOAD_REQUEST | <code>String</code> | <p>Describes the action to start the request.</p> |
| LOAD_SUCCEEDED | <code>String</code> | <p>Describes the action when an request has been successful.</p> |
| load | <code>function</code> | <p>Initial action dispatcher to start a request.</p> |
| loadSucceeded | <code>function</code> | <p>Action dispatcher when request was successful.</p> |
| loadFailed | <code>function</code> | <p>Action dispatcher when request was unsuccessful.</p> |
| loadCanceld | <code>function</code> | <p>Action dispatcher when request was canceld.</p> |
| loadCleared | <code>function</code> | <p>Action dispatcher to clear the current state.</p> |


* [requestAPIActions](#requestAPIActions)
    * [.load(requestId, payload)](#requestAPIActions.load) ⇒ <code>Object</code>
    * [.loadSucceeded(requestId, payload, meta)](#requestAPIActions.loadSucceeded) ⇒ <code>Object</code>
    * [.loadFailed(requestId, payload, meta)](#requestAPIActions.loadFailed) ⇒ <code>Object</code>
    * [.loadCanceld(requestId, payload)](#requestAPIActions.loadCanceld) ⇒ <code>Object</code>
    * [.loadCleared(requestId)](#requestAPIActions.loadCleared) ⇒ <code>Object</code>
    * [.loadPlain(requestId, payload)](#requestAPIActions.loadPlain) ⇒ <code>Object</code>

<a name="requestAPIActions.load"></a>

### requestAPIActions.load(requestId, payload) ⇒ <code>Object</code>
<p>Redux action to dispatch the initial request to the URL and
by using the given config.</p>

**Kind**: static method of [<code>requestAPIActions</code>](#requestAPIActions)  
**Returns**: <code>Object</code> - <p>FSA&lt;string, {}, RequestApiMeta&gt;</p>  

| Param | Type | Description |
| --- | --- | --- |
| requestId | <code>String</code> | <p>Id that describes the dispached request</p> |
| payload | <code>Object</code> | <p>Url and Config for Axios promise</p> |

<a name="requestAPIActions.loadSucceeded"></a>

### requestAPIActions.loadSucceeded(requestId, payload, meta) ⇒ <code>Object</code>
<p>Redux action to dipatch a successful request.</p>

**Kind**: static method of [<code>requestAPIActions</code>](#requestAPIActions)  
**Returns**: <code>Object</code> - <p>FSA&lt;string, ApiResult, ApiMeta&gt;</p>  

| Param | Type | Description |
| --- | --- | --- |
| requestId | <code>String</code> | <p>Id that describes the dispached request</p> |
| payload | <code>Object</code> | <p>An Object that holds the result from previos reuqest</p> |
| meta | <code>Object</code> | <p>An Object that holds the Url and the Config of the initial dispatch</p> |

<a name="requestAPIActions.loadFailed"></a>

### requestAPIActions.loadFailed(requestId, payload, meta) ⇒ <code>Object</code>
<p>Redux action to for a failed request.</p>

**Kind**: static method of [<code>requestAPIActions</code>](#requestAPIActions)  
**Returns**: <code>Object</code> - <p>FSA&lt;string, ApiResult, ApiMeta&gt;</p>  

| Param | Type | Description |
| --- | --- | --- |
| requestId | <code>String</code> | <p>Id that describes the dispached request</p> |
| payload | <code>Object</code> | <p>An Object that holds the error if previos reuqest failed</p> |
| meta | <code>Object</code> | <p>An Object that holds the Url and the Config of the initial dispatch</p> |

<a name="requestAPIActions.loadCanceld"></a>

### requestAPIActions.loadCanceld(requestId, payload) ⇒ <code>Object</code>
<p>Redux action to cancel a request.</p>

**Kind**: static method of [<code>requestAPIActions</code>](#requestAPIActions)  
**Returns**: <code>Object</code> - <p>FSA&lt;string, ApiResult, ApiMeta&gt;</p>  

| Param | Type | Description |
| --- | --- | --- |
| requestId | <code>String</code> | <p>Id that describes the dispached request</p> |
| payload | <code>Object</code> |  |

<a name="requestAPIActions.loadCleared"></a>

### requestAPIActions.loadCleared(requestId) ⇒ <code>Object</code>
<p>Redux action to clear the state</p>

**Kind**: static method of [<code>requestAPIActions</code>](#requestAPIActions)  
**Returns**: <code>Object</code> - <p>FSA&lt;string, {}, ApiMeta&gt;</p>  

| Param | Type | Description |
| --- | --- | --- |
| requestId | <code>string</code> | <p>Id that describes the dispached request</p> |

<a name="requestAPIActions.loadPlain"></a>

### requestAPIActions.loadPlain(requestId, payload) ⇒ <code>Object</code>
<p>Redux action to add a plain object from prev result to the state the state</p>

**Kind**: static method of [<code>requestAPIActions</code>](#requestAPIActions)  
**Returns**: <code>Object</code> - <p>FSA&lt;string, {}, ApiMeta&gt;</p>  

| Param | Type | Description |
| --- | --- | --- |
| requestId | <code>string</code> | <p>Id that describes the dispached request</p> |
| payload | <code>Object</code> | <p>object from prev result;</p> |

<a name="unsuccessfulAuthenticated"></a>

## unsuccessfulAuthenticated(void) ⇒
**Kind**: global function  
**Returns**: <p>AuthenticatorUnsuccessfulAction</p>  

| Param |
| --- |
| void | 

<a name="successfulAuthenticated"></a>

## successfulAuthenticated(void) ⇒
**Kind**: global function  
**Returns**: <p>AuthenticatorAction</p>  

| Param |
| --- |
| void | 

<a name="loadAuthentification"></a>

## loadAuthentification(payload) ⇒
<p>Action to load login request</p>

**Kind**: global function  
**Returns**: <p>AuthentificationAction</p>  

| Param |
| --- |
| payload | 

<a name="succeededAuthentification"></a>

## succeededAuthentification(payload) ⇒
<p>Action to dispatch succsseful Login</p>

**Kind**: global function  
**Returns**: <p>AuthentificationAction</p>  

| Param |
| --- |
| payload | 

<a name="failedAuthentification"></a>

## failedAuthentification(payload) ⇒
**Kind**: global function  
**Returns**: <p>AuthentificationAction</p>  

| Param |
| --- |
| payload | 

<a name="loadAccessTokenValidation"></a>

## loadAccessTokenValidation(accessToken) ⇒ <code>Object</code>
**Kind**: global function  

| Param | Type |
| --- | --- |
| accessToken | <code>AccessToken</code> | 

<a name="succeededAccessTokenValidation"></a>

## succeededAccessTokenValidation(accessToken) ⇒ <code>Object</code>
**Kind**: global function  

| Param | Type |
| --- | --- |
| accessToken | <code>AccessToken</code> | 

<a name="failedAccessTokenValidation"></a>

## failedAccessTokenValidation() ⇒ <code>Object</code>
**Kind**: global function  
<a name="saveAccessTokenToCookie"></a>

## saveAccessTokenToCookie(accessToken) ⇒
<p>Save access token to cookie</p>

**Kind**: global function  
**Returns**: <p>void;</p>  

| Param |
| --- |
| accessToken | 

<a name="removeAccessTokenFromCookie"></a>

## removeAccessTokenFromCookie()
<p>Remove access token cookie</p>

**Kind**: global function  
<a name="fetchAuthentification"></a>

## fetchAuthentification()
<p>Redux-logic to fetch user login</p>

**Kind**: global function  
<a name="fetchLogout"></a>

## fetchLogout()
<p>Redux logic to fetch logout</p>

**Kind**: global function  
<a name="validateAccessToken"></a>

## validateAccessToken()
<p>Redux logic to validate an existing token,</p>

**Kind**: global function  
<a name="createAuthentificationReducer"></a>

## createAuthentificationReducer(state, action) ⇒
**Kind**: global function  
**Returns**: <p>AuthentificationState</p>  

| Param |
| --- |
| state | 
| action | 

<a name="loadClientInfo"></a>

## loadClientInfo() ⇒
<p>Action to load client info</p>

**Kind**: global function  
**Returns**: <p>FSA</p>  
<a name="succeededClientInfo"></a>

## succeededClientInfo(payload) ⇒
<p>Action to load client info</p>

**Kind**: global function  
**Returns**: <p>FSA</p>  

| Param |
| --- |
| payload | 

<a name="failedClientInfo"></a>

## failedClientInfo(payload) ⇒
<p>Action to load client info</p>

**Kind**: global function  
**Returns**: <p>FSA</p>  

| Param |
| --- |
| payload | 

<a name="cancelClientInfo"></a>

## cancelClientInfo(payload) ⇒
<p>Action to load client info</p>

**Kind**: global function  
**Returns**: <p>FSA</p>  

| Param |
| --- |
| payload | 

<a name="clearClientInfo"></a>

## clearClientInfo(payload) ⇒
<p>Action to load client info</p>

**Kind**: global function  
**Returns**: <p>FSA</p>  

| Param |
| --- |
| payload | 

<a name="createRequestLogic"></a>

## createRequestLogic(requestId, appSettings)
<p>Default redux logic that inits when the <code>requestAPIActions.load</code> action is dispached.</p>

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| requestId | <code>string</code> | <p>The Id that describes the current logic</p> |
| appSettings | <code>Object</code> | <p>Setting that will be passed to the HTTP Service</p> |

<a name="createRequestLogic..logic"></a>

### createRequestLogic~logic
**Kind**: inner property of [<code>createRequestLogic</code>](#createRequestLogic)  
<a name="createRequestReducer"></a>

## createRequestReducer(requestId) ⇒ <code>object</code>
<p>Function to create the reducer for each provided request.</p>

**Kind**: global function  
**Returns**: <code>object</code> - <p>The redux state of the reducer</p>  

| Param | Type | Description |
| --- | --- | --- |
| requestId | <code>string</code> | <p>The id for the created reducer</p> |

<a name="createRequestReducer..reducer"></a>

### createRequestReducer~reducer(state, action)
<p>The actual redux reducer</p>

**Kind**: inner method of [<code>createRequestReducer</code>](#createRequestReducer)  

| Param | Type |
| --- | --- |
| state | <code>object</code> | 
| action | <code>object</code> | 

<a name="createRequestActionType"></a>

## createRequestActionType(defaultType, extendedType) ⇒ <code>String</code>
<p>Function to create the the Redux actions type that will be dispatched by each state when
calling an action.</p>

**Kind**: global function  
**Returns**: <code>String</code> - <p>Redux action type</p>  

| Param | Type |
| --- | --- |
| defaultType | <code>String</code> | 
| extendedType | <code>String</code> | 

<a name="getAccessTokenFromCookie"></a>

## getAccessTokenFromCookie(ACCESSTOKEN_NAME) ⇒ <code>Object</code>
<p>Get access token from cookie</p>

**Kind**: global function  
**Returns**: <code>Object</code> - <p>AccessToken</p>  

| Param | Type |
| --- | --- |
| ACCESSTOKEN_NAME | <code>String</code> | 

<a name="clearStateByListOfKeys"></a>

## clearStateByListOfKeys(keys, props) ⇒
<p>Clears a list of states stored in global state by given keys.</p>

**Kind**: global function  
**Returns**: <p>Function</p>  

| Param | Type | Description |
| --- | --- | --- |
| keys | <code>Array</code> | <p>List of keys</p> |
| props | <code>ClearStateProps</code> | <p>Object that holds all information for clearing states</p> |

<a name="clearRequestStates"></a>

## clearRequestStates(props)
<p>Clears one state bay given key</p>

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| props | <code>ClearStateProps</code> | <p>Object that holds all information for clearing states</p> |

