"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var request_action_1 = require("./request.action");
var utils_1 = require("../utils");
/**
 * Function to create the reducer for each provided request.
 *
 * @param {string} requestId The id for the created reducer
 * @returns {object} The redux state of the reducer
 */
function createRequestReducer(requestId) {
    var initialState = {
        error: undefined,
        loaded: false,
        meta: undefined,
        result: undefined,
        updating: false,
    };
    /**
     * The actual redux reducer
     * @param {object} state
     * @param {object}
     */
    function reducer(state, action) {
        if (state === void 0) { state = initialState; }
        var updateState = (action.meta && action.meta.update) ||
            (action.meta && action.meta.options && action.meta.options.update) ?
            true : false;
        var isPager = action.meta && action.meta.options && action.meta.options.isPager ? true : false;
        var isSearch = action.meta && action.meta.options && action.meta.options.isSearch ? true : false;
        switch (action.type) {
            case utils_1.createRequestActionType(request_action_1.requestAPIActions.LOAD_REQUEST, requestId):
                return {
                    error: undefined,
                    loaded: false,
                    meta: {
                        id: action.meta ? action.meta.id : '',
                        options: action.payload ? action.payload : undefined,
                        update: action.meta && action.meta.update,
                        isPager: isPager,
                        isSearch: isSearch,
                    },
                    result: updateState ? state.result : undefined,
                    updating: true,
                };
            case utils_1.createRequestActionType(request_action_1.requestAPIActions.LOAD_SUCCEEDED, requestId):
                return {
                    error: undefined,
                    loaded: true,
                    meta: action.meta,
                    result: action.payload,
                    updating: false,
                };
            case utils_1.createRequestActionType(request_action_1.requestAPIActions.LOAD_UPDATE, requestId):
                if (action.meta && action.meta.isSearch) {
                    var resultFromSearch = utils_1.updateResultFromSearch(state, action.payload && action.payload);
                    return {
                        error: undefined,
                        loaded: true,
                        meta: action.meta,
                        result: resultFromSearch,
                        updating: false,
                    };
                }
                var resultFromRow = utils_1.updateResultFromRow(state, action.payload && action.payload);
                return {
                    error: undefined,
                    loaded: true,
                    meta: action.meta,
                    result: resultFromRow,
                    updating: false,
                };
            case utils_1.createRequestActionType(request_action_1.requestAPIActions.LOAD_FAILED, requestId):
                return {
                    error: action.payload ? action.payload : undefined,
                    loaded: false,
                    meta: action.meta,
                    result: (updateState && state.result) || undefined,
                    updating: false,
                };
            case utils_1.createRequestActionType(request_action_1.requestAPIActions.LOAD_PLAIN, requestId):
                return {
                    error: undefined,
                    loaded: true,
                    meta: action.meta,
                    result: action.payload,
                    updating: false,
                };
            case utils_1.createRequestActionType(request_action_1.requestAPIActions.LOAD_CANCELD, requestId):
                return state;
            case utils_1.createRequestActionType(request_action_1.requestAPIActions.LOAD_CLEARED, requestId):
                return initialState;
            default:
                return state;
        }
    }
    ;
    return reducer;
}
exports.createRequestReducer = createRequestReducer;
//# sourceMappingURL=request.reducer.js.map