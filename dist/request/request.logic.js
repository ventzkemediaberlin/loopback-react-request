"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var redux_logic_1 = require("redux-logic");
var request_action_1 = require("./request.action");
var service_1 = require("../service");
var utils_1 = require("../utils");
var authenticator_1 = require("../authenticator");
// type FSA = FluxStandardAction<{}, RequestApiHttpPayload>;
/**
 * Default redux logic that inits when the `requestAPIActions.load` action is dispached.
 * @param {string} requestId The Id that describes the current logic
 * @param {Object} appSettings Setting that will be passed to the HTTP Service
 */
exports.createRequestLogic = function (requestId, appSettings, reducer) {
    /**
     *
     */
    var logic = redux_logic_1.createLogic({
        type: utils_1.createRequestActionType(request_action_1.requestAPIActions.LOAD_REQUEST, requestId),
        cancelType: utils_1.createRequestActionType(request_action_1.requestAPIActions.LOAD_CANCELD, requestId),
        latest: true,
        process: function (process, dispatcher, done) {
            var action = process.action;
            var meta = {
                id: requestId,
                options: action.payload,
            };
            var dispatch = dispatcher;
            return new service_1.HttpService(action.payload, appSettings).fetch()
                .then(function (value) {
                var data = value.data, status = value.status, statusText = value.statusText;
                var result = data;
                meta.status = status;
                meta.statusText = statusText;
                meta.isSearch = meta.options && meta.options.isSearch;
                if (action.payload && meta && meta.options && meta.options.isPager) {
                    meta.isPager = action.payload.isPager;
                    dispatch(request_action_1.requestAPIActions.loadUpdate(requestId, result, meta));
                }
                else {
                    dispatch(request_action_1.requestAPIActions.loadSucceeded(requestId, result, meta));
                }
            })
                .catch(function (error) {
                var data = utils_1.errorNotFound(error);
                meta.status = error.response && error.response.status || data && data.statusCode;
                meta.statusText = error.response && error.response.statusText || data && data.message;
                meta.update = action.meta && action.meta.update || false;
                if (meta.status === 401 && meta.statusText === 'Unauthorized') {
                    dispatch(authenticator_1.unsuccessfulAuthenticated());
                    dispatch(authenticator_1.clearAuthentification());
                    reducer.map(function (state) { return dispatch(request_action_1.requestAPIActions.loadCleared(state)); });
                }
                dispatch(request_action_1.requestAPIActions.loadFailed(requestId, data, meta));
            })
                .then(function () { return done(); });
        }
    });
    return logic;
};
//# sourceMappingURL=request.logic.js.map