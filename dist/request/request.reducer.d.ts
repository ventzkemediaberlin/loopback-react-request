import { RequestApiResult, RequestApiMeta } from '../types';
import { FluxStandardAction } from 'flux-standard-action';
/**
 * Function to create the reducer for each provided request.
 *
 * @param {string} requestId The id for the created reducer
 * @returns {object} The redux state of the reducer
 */
export declare function createRequestReducer<ApiResult>(requestId: string): (state: RequestApiResult<ApiResult> | undefined, action: FluxStandardAction<string, RequestApiResult<ApiResult>, RequestApiMeta>) => RequestApiResult<ApiResult>;
