import { RequestApiPayload, RequestApiMeta } from '../types';
/**
 * This is an Object that holds the Redux action types and action functions to dispatch
 * actions from React or Redux logic to act with the redux state.
 * @property {String} LOAD_CANCELD Describes the action when an request is canceld.
 * @property {String} LOAD_CLEARED Describes the action to clear the state.
 * @property {String} LOAD_FAILED Describes the action when an request has failed.
 * @property {String} LOAD_REQUEST Describes the action to start the request.
 * @property {String} LOAD_SUCCEEDED Describes the action when an request has been successful.
 * @property {Function} load Initial action dispatcher to start a request.
 * @property {Function} loadSucceeded Action dispatcher when request was successful.
 * @property {Function} loadFailed Action dispatcher when request was unsuccessful.
 * @property {Function} loadCanceld Action dispatcher when request was canceld.
 * @property {Function} loadCleared Action dispatcher to clear the current state.
 */
export declare const requestAPIActions: {
    LOAD_CANCELD: string;
    LOAD_CLEARED: string;
    LOAD_FAILED: string;
    LOAD_REQUEST: string;
    LOAD_SUCCEEDED: string;
    LOAD_UPDATE: string;
    LOAD_PLAIN: string;
    /**
     * Redux action to dispatch the initial request to the URL and
     * by using the given config.
     * @alias requestAPIActions.load
     * @param {String} requestId Id that describes the dispached request
     * @param {Object} payload Url and Config for Axios promise
     * @returns {Object} FSA<string, {}, RequestApiMeta>
     */
    load<Payload>(requestId: string, payload: Payload, update?: boolean): import("flux-standard-action").FluxStandardAction<string, {}, RequestApiMeta>;
    /**
     * Redux action to dipatch a successful request.
     * @alias requestAPIActions.loadSucceeded
     * @param {String} requestId Id that describes the dispached request
     * @param {Object} payload An Object that holds the result from previos reuqest
     * @param {Object} meta An Object that holds the Url and the Config of the initial dispatch
     * @returns {Object} FSA<string, ApiResult, ApiMeta>
     */
    loadSucceeded<ApiResult, ApiMeta extends RequestApiMeta>(requestId: string, payload: RequestApiPayload, meta: ApiMeta): import("flux-standard-action").FluxStandardAction<string, ApiResult, ApiMeta>;
    /**
     * Redux action to for a failed request.
     * @alias requestAPIActions.loadFailed
     * @param {String} requestId Id that describes the dispached request
     * @param {Object} payload An Object that holds the error if previos reuqest failed
     * @param {Object} meta An Object that holds the Url and the Config of the initial dispatch
     * @returns {Object} FSA<string, ApiResult, ApiMeta>
     */
    loadFailed<ApiResult, ApiMeta extends RequestApiMeta>(requestId: string, payload: ApiResult, meta: ApiMeta): import("flux-standard-action").FluxStandardAction<string, ApiResult, ApiMeta>;
    loadUpdate<ApiResult, ApiMeta extends RequestApiMeta>(requestId: string, payload: RequestApiPayload, meta: ApiMeta): import("flux-standard-action").FluxStandardAction<string, ApiResult, ApiMeta>;
    /**
     * Redux action to cancel a request.
     * @alias requestAPIActions.loadCanceld
     * @param {String} requestId Id that describes the dispached request
     * @param {Object} payload
     * @returns {Object} FSA<string, ApiResult, ApiMeta>
     */
    loadCanceld<ApiResult, ApiMeta extends RequestApiMeta>(requestId: string, payload?: ApiResult | undefined): import("flux-standard-action").FluxStandardAction<string, ApiResult, ApiMeta>;
    /**
     * Redux action to clear the state
     * @alias requestAPIActions.loadCleared
     * @param {string} requestId Id that describes the dispached request
     * @returns {Object} FSA<string, {}, ApiMeta>
     */
    loadCleared(requestId: string): import("flux-standard-action").FluxStandardAction<string, {}, RequestApiMeta>;
    /**
     * Redux action to add a plain object from prev result to the state the state
     * @alias requestAPIActions.loadPlain
     * @param {string} requestId Id that describes the dispached request
     * @param {Object} payload object from prev result;
     * @returns {Object} FSA<string, {}, ApiMeta>
     */
    loadPlain<Model extends Object>(requestId: string, payload: Model): import("flux-standard-action").FluxStandardAction<string, Model, RequestApiMeta>;
};
