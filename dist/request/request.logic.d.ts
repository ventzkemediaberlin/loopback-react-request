import { AppSettings } from '../types';
/**
 * Default redux logic that inits when the `requestAPIActions.load` action is dispached.
 * @param {string} requestId The Id that describes the current logic
 * @param {Object} appSettings Setting that will be passed to the HTTP Service
 */
export declare const createRequestLogic: <ApiResult>(requestId: string, appSettings: AppSettings, reducer: string[]) => import("redux-logic/definitions/utilities").Override<import("redux-logic/definitions/logic").CreateLogic.Config.Base<{}, import("redux-logic").Action<string, undefined, undefined>, string> & import("redux-logic/definitions/logic").CreateLogic.Config.Validate<{}, import("redux-logic").Action<string, undefined, undefined>, {}, undefined> & import("redux-logic/definitions/logic").CreateLogic.Config.Process<{}, import("redux-logic").Action<string, undefined, undefined>, {}, undefined>, {
    name: string;
    type: string;
    cancelType: string;
}> | import("redux-logic/definitions/utilities").Override<import("redux-logic/definitions/logic").CreateLogic.Config.Base<{}, import("redux-logic").Action<string, undefined, undefined>, string> & import("redux-logic/definitions/logic").CreateLogic.Config.Transform<{}, import("redux-logic").Action<string, undefined, undefined>, {}, undefined> & import("redux-logic/definitions/logic").CreateLogic.Config.Process<{}, import("redux-logic").Action<string, undefined, undefined>, {}, undefined>, {
    name: string;
    type: string;
    cancelType: string;
}>;
