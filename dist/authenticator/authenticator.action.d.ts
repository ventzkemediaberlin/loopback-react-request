import { AuthentificationData, AuthentificationAction, AccessToken, RequestApiError, LogoutAction } from '../types';
export declare const LOADING_VALIDATION = "LOADING_VALIDATION";
export declare const SUCCESSFUL_VALIDATION = "SUCCESSFUL_VALIDATION";
export declare const FAILED_LOGIN = "FAILED_LOGIN";
export declare const CANCEL_VALIDATION = "CANCEL_VALIDATION";
export declare const CLEAR_VALIDATION = "CLEAR_VALIDATION";
export declare const SUCCESSFUL_AUTHENTICATED = "SUCCESSFUL_AUTHENTICATED";
export declare const UNSUCCESSFUL_AUTHENTICATED = "UNSUCCESSFUL_AUTHENTICATED";
export declare const LOADING_LOGIN: string;
export declare const SUCCEEDED_LOGIN: string;
export declare const FAILED_VALIDATION: string;
export declare const CANCEL_LOGIN: string;
export declare const CLEAR_LOGIN: string;
export declare const LOADING_LOGOUT: string;
export declare const SUCCEEDED_LOGOUT: string;
export declare const FAILED_LOGOUT: string;
export declare const CANCEL_LOGOUT: string;
export declare const CLEAR_LOGOUT: string;
export declare type CANCEL_LOGIN_TYPE = 'CANCEL_LOGIN';
export declare type CANCEL_VALIDATION = 'CANCEL_VALIDATION';
export declare type CLEAR_LOGIN_TYPE = 'CLEAR_LOGIN';
export declare type CLEAR_LOGOUT_TYPE = 'CLEAR_LOGOUT';
export declare type CLEAR_VALIDATION = 'CLEAR_VALIDATION';
export declare type CANCEL_LOGOUT_TYPE = 'CANCEL_LOGOUT';
export declare type FAILED_LOGIN_TYPE = 'FAILED_LOGIN';
export declare type FAILED_LOGOUT_TYPE = 'FAILED_LOGOUT';
export declare type FAILED_VALIDATION = 'FAILED_VALIDATION';
export declare type LOADING_LOGIN_TYPE = 'LOADING_LOGIN';
export declare type LOADING_LOGOUT_TYPE = 'LOADING_LOGOUT';
export declare type LOADING_VALIDATION = 'LOADING_VALIDATION';
export declare type SUCCESSFUL_AUTHENTICATED_TYPE = 'SUCCESSFUL_AUTHENTICATED';
export declare type SUCCESSFUL_VALIDATION = 'SUCCESSFUL_VALIDATION';
export declare type SUCCEEDED_LOGIN_TYPE = 'SUCCEEDED_LOGIN';
export declare type SUCCEEDED_LOGOUT_TYPE = 'SUCCEEDED_LOGOUT';
export declare type UNSUCCESSFUL_AUTHENTICATED_TYPE = 'UNSUCCESSFUL_AUTHENTICATED';
export interface ValidateAccessTokenReturn {
    type: LOADING_VALIDATION | SUCCESSFUL_VALIDATION;
    payload: AccessToken;
}
export interface AuthenticatorAction {
    type: SUCCESSFUL_AUTHENTICATED_TYPE | UNSUCCESSFUL_AUTHENTICATED_TYPE;
    payload?: AccessToken;
}
/**
 * @param void
 * @returns AuthenticatorUnsuccessfulAction
 */
export declare function unsuccessfulAuthenticated(): AuthenticatorAction;
/**
 * @param void
 * @returns AuthenticatorAction
 */
export declare function successfulAuthenticated(payload: AccessToken): AuthenticatorAction;
/**
 * Action to load login request
 * @param payload
 * @returns AuthentificationAction
 */
export declare function loadAuthentification(payload: AuthentificationData): AuthentificationAction;
/**
 * Action to dispatch succsseful Login
 * @param payload
 * @returns AuthentificationAction
 */
export declare function succeededAuthentification(payload: AccessToken): AuthentificationAction;
/**
 *
 * @param payload
 * @returns AuthentificationAction
 */
export declare function failedAuthentification(payload: RequestApiError): AuthentificationAction;
export declare function clearAuthentification(): AuthentificationAction;
export declare function loadLogout(payload?: AccessToken): LogoutAction;
export declare function succeededLogout(): LogoutAction;
export declare function failedLogout(payload?: RequestApiError): LogoutAction;
export declare function clearLogout(): LogoutAction;
/**
 *
 * @param {AccessToken} accessToken
 * @returns {Object}
 */
export declare function loadAccessTokenValidation(accessToken: AccessToken): ValidateAccessTokenReturn;
/**
 *
 * @param {AccessToken} accessToken
 * @returns {Object}
 */
export declare function succeededAccessTokenValidation(accessToken: AccessToken): ValidateAccessTokenReturn;
/**
 *
 * @returns {Object}
 */
export declare function failedAccessTokenValidation(error: RequestApiError): {
    type: string;
    payload: RequestApiError;
};
export declare function clearAccessTokenValidation(): {
    type: string;
};
