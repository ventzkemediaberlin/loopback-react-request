import { AccessToken, AppSettings, ReducerIds } from '../types';
/**
 * Save access token to cookie
 * @param accessToken
 * @returns void;
 */
export declare function saveAccessTokenToCookie(accessToken: AccessToken, name: string): void;
/**
 * Remove access token cookie
 */
export declare function removeAccessTokenFromCookie(name: string): void;
export declare const creatAuthenticationLogic: (reducer: ReducerIds, appSettings: AppSettings) => {
    /**
     * Redux-logic to fetch user login
     */
    fetchAuthentification: () => import("redux-logic/definitions/utilities").Override<import("redux-logic/definitions/logic").CreateLogic.Config.Base<any, import("redux-logic").Action<string, Object, undefined>, string> & import("redux-logic/definitions/logic").CreateLogic.Config.Validate<any, import("redux-logic").Action<string, Object, undefined>, {}, undefined> & import("redux-logic/definitions/logic").CreateLogic.Config.Process<any, import("redux-logic").Action<string, Object, undefined>, {}, undefined>, {
        name: string;
        type: string;
        cancelType: string;
    }> | import("redux-logic/definitions/utilities").Override<import("redux-logic/definitions/logic").CreateLogic.Config.Base<any, import("redux-logic").Action<string, Object, undefined>, string> & import("redux-logic/definitions/logic").CreateLogic.Config.Transform<any, import("redux-logic").Action<string, Object, undefined>, {}, undefined> & import("redux-logic/definitions/logic").CreateLogic.Config.Process<any, import("redux-logic").Action<string, Object, undefined>, {}, undefined>, {
        name: string;
        type: string;
        cancelType: string;
    }>;
    /**
     * Redux logic to fetch logout
     */
    fetchLogout: () => import("redux-logic/definitions/utilities").Override<import("redux-logic/definitions/logic").CreateLogic.Config.Base<any, import("redux-logic").Action<string, AccessToken, undefined>, string> & import("redux-logic/definitions/logic").CreateLogic.Config.Validate<any, import("redux-logic").Action<string, AccessToken, undefined>, {}, undefined> & import("redux-logic/definitions/logic").CreateLogic.Config.Process<any, import("redux-logic").Action<string, AccessToken, undefined>, {}, undefined>, {
        name: string;
        type: string;
        cancelType: string;
    }> | import("redux-logic/definitions/utilities").Override<import("redux-logic/definitions/logic").CreateLogic.Config.Base<any, import("redux-logic").Action<string, AccessToken, undefined>, string> & import("redux-logic/definitions/logic").CreateLogic.Config.Transform<any, import("redux-logic").Action<string, AccessToken, undefined>, {}, undefined> & import("redux-logic/definitions/logic").CreateLogic.Config.Process<any, import("redux-logic").Action<string, AccessToken, undefined>, {}, undefined>, {
        name: string;
        type: string;
        cancelType: string;
    }>;
    /**
     * Redux logic to validate an existing token,
     */
    validateAccessToken: () => import("redux-logic/definitions/utilities").Override<import("redux-logic/definitions/logic").CreateLogic.Config.Base<any, import("redux-logic").Action<string, AccessToken, undefined>, string> & import("redux-logic/definitions/logic").CreateLogic.Config.Validate<any, import("redux-logic").Action<string, AccessToken, undefined>, {}, undefined> & import("redux-logic/definitions/logic").CreateLogic.Config.Process<any, import("redux-logic").Action<string, AccessToken, undefined>, {}, undefined>, {
        name: string;
        type: string;
        cancelType: string;
    }> | import("redux-logic/definitions/utilities").Override<import("redux-logic/definitions/logic").CreateLogic.Config.Base<any, import("redux-logic").Action<string, AccessToken, undefined>, string> & import("redux-logic/definitions/logic").CreateLogic.Config.Transform<any, import("redux-logic").Action<string, AccessToken, undefined>, {}, undefined> & import("redux-logic/definitions/logic").CreateLogic.Config.Process<any, import("redux-logic").Action<string, AccessToken, undefined>, {}, undefined>, {
        name: string;
        type: string;
        cancelType: string;
    }>;
};
