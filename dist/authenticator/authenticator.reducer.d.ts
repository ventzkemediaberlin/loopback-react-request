import { AuthentificationAction, AuthentificationState, IsAuthentificatedState, LogoutAction, LogoutState, RequestApiError, AccessToken, AppSettings, ValidateTokenState } from '../types';
import { AuthenticatorAction } from './authenticator.action';
export declare const createAuthenticatorReducer: (appSettings: AppSettings) => (state: IsAuthentificatedState | undefined, action: AuthenticatorAction) => IsAuthentificatedState;
/**
 *
 * @param state
 * @param action
 * @returns AuthentificationState
 */
export declare const createAuthentificationReducer: (appSettings: AppSettings) => (state: AuthentificationState | undefined, action: AuthentificationAction) => AuthentificationState;
export declare const createLogoutReducer: () => (state: LogoutState | undefined, action: LogoutAction) => LogoutState;
export declare const accessTokenState: ValidateTokenState;
export declare const createValidateAccessTokenReducer: () => (state: ValidateTokenState | undefined, action: {
    type: string;
    payload?: RequestApiError | AccessToken | undefined;
}) => ValidateTokenState;
