"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("../utils");
exports.LOADING_VALIDATION = 'LOADING_VALIDATION';
exports.SUCCESSFUL_VALIDATION = 'SUCCESSFUL_VALIDATION';
exports.FAILED_LOGIN = 'FAILED_LOGIN';
exports.CANCEL_VALIDATION = 'CANCEL_VALIDATION';
exports.CLEAR_VALIDATION = 'CLEAR_VALIDATION';
exports.SUCCESSFUL_AUTHENTICATED = 'SUCCESSFUL_AUTHENTICATED';
exports.UNSUCCESSFUL_AUTHENTICATED = 'UNSUCCESSFUL_AUTHENTICATED';
exports.LOADING_LOGIN = utils_1.createRequestActionType('LOADING', 'login');
exports.SUCCEEDED_LOGIN = utils_1.createRequestActionType('SUCCEEDED', 'login');
exports.FAILED_VALIDATION = utils_1.createRequestActionType('FAILED', 'login');
exports.CANCEL_LOGIN = utils_1.createRequestActionType('CANCEL', 'login');
exports.CLEAR_LOGIN = utils_1.createRequestActionType('CLEAR', 'login');
exports.LOADING_LOGOUT = utils_1.createRequestActionType('LOADING', 'logout');
exports.SUCCEEDED_LOGOUT = utils_1.createRequestActionType('SUCCEEDED', 'logout');
exports.FAILED_LOGOUT = utils_1.createRequestActionType('FAILED', 'logout');
exports.CANCEL_LOGOUT = utils_1.createRequestActionType('CANCEL', 'logout');
exports.CLEAR_LOGOUT = utils_1.createRequestActionType('CLEAR', 'logout');
/**
 * @param void
 * @returns AuthenticatorUnsuccessfulAction
 */
function unsuccessfulAuthenticated() {
    return {
        type: exports.UNSUCCESSFUL_AUTHENTICATED
    };
}
exports.unsuccessfulAuthenticated = unsuccessfulAuthenticated;
/**
 * @param void
 * @returns AuthenticatorAction
 */
function successfulAuthenticated(payload) {
    return {
        type: exports.SUCCESSFUL_AUTHENTICATED,
        payload: payload,
    };
}
exports.successfulAuthenticated = successfulAuthenticated;
/**
 * Action to load login request
 * @param payload
 * @returns AuthentificationAction
 */
function loadAuthentification(payload) {
    return {
        type: exports.LOADING_LOGIN,
        payload: payload,
    };
}
exports.loadAuthentification = loadAuthentification;
/**
 * Action to dispatch succsseful Login
 * @param payload
 * @returns AuthentificationAction
 */
function succeededAuthentification(payload) {
    return {
        type: exports.SUCCEEDED_LOGIN,
        payload: payload,
    };
}
exports.succeededAuthentification = succeededAuthentification;
/**
 *
 * @param payload
 * @returns AuthentificationAction
 */
function failedAuthentification(payload) {
    return {
        type: exports.FAILED_LOGIN,
        payload: payload,
    };
}
exports.failedAuthentification = failedAuthentification;
function clearAuthentification() {
    return {
        type: exports.CLEAR_LOGIN,
        payload: undefined,
    };
}
exports.clearAuthentification = clearAuthentification;
function loadLogout(payload) {
    return {
        type: exports.LOADING_LOGOUT,
        payload: payload,
    };
}
exports.loadLogout = loadLogout;
function succeededLogout() {
    return {
        type: exports.SUCCEEDED_LOGOUT,
    };
}
exports.succeededLogout = succeededLogout;
function failedLogout(payload) {
    return {
        type: exports.FAILED_LOGOUT,
        payload: payload,
    };
}
exports.failedLogout = failedLogout;
function clearLogout() {
    return {
        type: exports.CLEAR_LOGOUT,
    };
}
exports.clearLogout = clearLogout;
/**
 *
 * @param {AccessToken} accessToken
 * @returns {Object}
 */
function loadAccessTokenValidation(accessToken) {
    return {
        type: exports.LOADING_VALIDATION,
        payload: accessToken,
    };
}
exports.loadAccessTokenValidation = loadAccessTokenValidation;
/**
 *
 * @param {AccessToken} accessToken
 * @returns {Object}
 */
function succeededAccessTokenValidation(accessToken) {
    return {
        type: exports.SUCCESSFUL_VALIDATION,
        payload: accessToken,
    };
}
exports.succeededAccessTokenValidation = succeededAccessTokenValidation;
/**
 *
 * @returns {Object}
 */
function failedAccessTokenValidation(error) {
    return {
        type: exports.FAILED_VALIDATION,
        payload: error,
    };
}
exports.failedAccessTokenValidation = failedAccessTokenValidation;
function clearAccessTokenValidation() {
    return {
        type: exports.CLEAR_VALIDATION,
    };
}
exports.clearAccessTokenValidation = clearAccessTokenValidation;
//# sourceMappingURL=authenticator.action.js.map