"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Cookie = require("js-cookie");
var redux_logic_1 = require("redux-logic");
var authenticator_action_1 = require("./authenticator.action");
var utils_1 = require("../utils");
var service_1 = require("../service");
var request_action_1 = require("../request/request.action");
/**
 * Save access token to cookie
 * @param accessToken
 * @returns void;
 */
function saveAccessTokenToCookie(accessToken, name) {
    var ttlInHours = accessToken.ttl / 60 / 60;
    var expires = ttlInHours / 24;
    Cookie.set(name, accessToken, { expires: expires, path: '/' });
}
exports.saveAccessTokenToCookie = saveAccessTokenToCookie;
/**
 * Remove access token cookie
 */
function removeAccessTokenFromCookie(name) {
    Cookie.remove(name, { path: '/', });
}
exports.removeAccessTokenFromCookie = removeAccessTokenFromCookie;
var setUrlToUserRemote = function (path) {
    return path && path.length ? path : '/Users';
};
exports.creatAuthenticationLogic = function (reducer, appSettings) { return ({
    /**
     * Redux-logic to fetch user login
     */
    fetchAuthentification: function () {
        var logic = redux_logic_1.createLogic({
            type: authenticator_action_1.LOADING_LOGIN,
            cancelType: authenticator_action_1.CANCEL_LOGIN,
            latest: true,
            process: function (_a, dispatcher, done) {
                var action = _a.action;
                var dispatch = dispatcher;
                return new service_1.HttpService({
                    options: {
                        method: 'POST',
                        data: action.payload,
                    },
                    url: setUrlToUserRemote(appSettings.userRemote) + "/login",
                }, appSettings)
                    .fetch()
                    .then(function (res) {
                    reducer.public.map(function (state) {
                        return dispatch(request_action_1.requestAPIActions.loadCleared(state));
                    });
                    return res;
                })
                    .then(function (res) {
                    saveAccessTokenToCookie(res.data, appSettings.cookieName);
                    return res;
                })
                    .then(function (res) {
                    dispatch(authenticator_action_1.successfulAuthenticated(res.data));
                    return res;
                })
                    .then(function (res) {
                    dispatch(authenticator_action_1.succeededAuthentification(res.data));
                    return res;
                })
                    .then(function () { return dispatch(authenticator_action_1.clearAccessTokenValidation()); })
                    .catch(function (data) {
                    var error = utils_1.errorNotFound(data);
                    dispatch(authenticator_action_1.failedAuthentification(error));
                })
                    .then(function () { return done(); });
            }
        });
        return logic;
    },
    /**
     * Redux logic to fetch logout
     */
    fetchLogout: function () {
        var logic = redux_logic_1.createLogic({
            type: authenticator_action_1.LOADING_LOGOUT,
            cancelType: authenticator_action_1.CANCEL_LOGOUT,
            latest: true,
            process: function (_a, dispatcher, done) {
                var action = _a.action;
                var dispatch = dispatcher;
                var accessToken = action.payload;
                return new service_1.HttpService({
                    options: {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'x-access-token': accessToken.id,
                        },
                    },
                    url: setUrlToUserRemote(appSettings.userRemote) + "/logout",
                }, appSettings)
                    .fetch()
                    .then(function (res) {
                    reducer.secure.map(function (state) {
                        return dispatch(request_action_1.requestAPIActions.loadCleared(state));
                    });
                    return res;
                })
                    .then(function (res) {
                    removeAccessTokenFromCookie(appSettings.cookieName);
                    return res;
                })
                    .then(function (res) {
                    dispatch(authenticator_action_1.unsuccessfulAuthenticated());
                    return res;
                })
                    .then(function (res) {
                    dispatch(authenticator_action_1.clearAuthentification());
                    return res;
                })
                    .then(function (res) {
                    dispatch(authenticator_action_1.clearAccessTokenValidation());
                    return res;
                })
                    .then(function () { return dispatch(authenticator_action_1.succeededLogout()); })
                    .then(function () { return dispatch(authenticator_action_1.clearLogout()); })
                    .catch(function (error) {
                    var data = utils_1.errorNotFound(error);
                    return dispatch(authenticator_action_1.failedLogout(data));
                })
                    .then(function () { return done(); });
            }
        });
        return logic;
    },
    /**
     * Redux logic to validate an existing token,
     */
    validateAccessToken: function () {
        var logic = redux_logic_1.createLogic({
            type: authenticator_action_1.LOADING_VALIDATION,
            cancelType: authenticator_action_1.CANCEL_VALIDATION,
            latest: true,
            process: function (_a, dispatcher, done) {
                var action = _a.action;
                var dispatch = dispatcher;
                var accessToken = action.payload;
                return new service_1.HttpService({
                    options: {
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'x-access-token': accessToken.id,
                        },
                    },
                    url: setUrlToUserRemote(appSettings.userRemote) + "/" + accessToken.userId + "/accessTokens/" + accessToken.id,
                }, appSettings)
                    .fetch()
                    .then(function (res) {
                    dispatch(authenticator_action_1.succeededAccessTokenValidation(res.data));
                    return res;
                })
                    .then(function (res) {
                    return dispatch(authenticator_action_1.succeededAuthentification(res.data));
                }).catch(function (error) {
                    var data = utils_1.errorNotFound(error);
                    return dispatch(authenticator_action_1.failedAccessTokenValidation(data));
                })
                    .then(function () { return done(); });
            },
        });
        return logic;
    }
}); };
//# sourceMappingURL=authenticator.logic.js.map