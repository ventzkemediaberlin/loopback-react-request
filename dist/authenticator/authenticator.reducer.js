"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var lodash_1 = require("lodash");
var utils_1 = require("../utils");
var authenticator_action_1 = require("./authenticator.action");
function isAuthentificatedState(name) {
    var token = utils_1.getAccessTokenFromCookie(name);
    return {
        accessToken: token,
        isAuthentificated: token && token.id.length ? true : false,
    };
}
exports.createAuthenticatorReducer = function (appSettings) { return function (state, action) {
    if (state === void 0) { state = isAuthentificatedState(appSettings.cookieName); }
    switch (action.type) {
        case authenticator_action_1.SUCCESSFUL_AUTHENTICATED:
            return lodash_1.assign({}, {
                accessToken: action.payload,
                isAuthentificated: true,
            });
        case authenticator_action_1.UNSUCCESSFUL_AUTHENTICATED:
            return lodash_1.assign({}, {
                accessToken: undefined,
                isAuthentificated: false,
            });
        default:
            return state;
    }
}; };
function authentificationState(name) {
    var token = utils_1.getAccessTokenFromCookie(name);
    return {
        accessToken: token,
        error: undefined,
        loaded: false,
        updating: false,
    };
}
/**
 *
 * @param state
 * @param action
 * @returns AuthentificationState
 */
exports.createAuthentificationReducer = function (appSettings) { return function (state, action) {
    if (state === void 0) { state = authentificationState(appSettings.cookieName); }
    switch (action.type) {
        case authenticator_action_1.LOADING_LOGIN:
            return lodash_1.assign({}, {
                accessToken: undefined,
                error: undefined,
                loaded: false,
                updating: true,
            });
        case authenticator_action_1.SUCCEEDED_LOGIN:
            return lodash_1.assign({}, {
                accessToken: action.payload,
                error: undefined,
                updating: false,
                loaded: true,
            });
        case authenticator_action_1.FAILED_LOGIN:
            return lodash_1.assign({}, {
                accessToken: undefined,
                error: action.payload,
                updating: false,
                loaded: true,
            });
        case authenticator_action_1.CANCEL_LOGIN:
            return state;
        case authenticator_action_1.CLEAR_LOGIN:
            return lodash_1.assign({}, {
                accessToken: undefined,
                error: undefined,
                updating: false,
                loaded: false,
            });
        default:
            return state;
    }
}; };
var logoutState = {
    error: undefined,
    loaded: false,
    updating: false,
};
exports.createLogoutReducer = function () { return function (state, action) {
    if (state === void 0) { state = logoutState; }
    switch (action.type) {
        case authenticator_action_1.LOADING_LOGOUT:
            return lodash_1.assign({}, {
                error: undefined,
                loaded: false,
                updating: true
            });
        case authenticator_action_1.SUCCEEDED_LOGOUT:
            return lodash_1.assign({}, {
                error: undefined,
                loaded: true,
                updating: false
            });
        case authenticator_action_1.FAILED_LOGOUT:
            return lodash_1.assign({}, {
                error: action.payload,
                loaded: false,
                updating: false
            });
        case authenticator_action_1.CLEAR_LOGOUT:
            return lodash_1.assign({}, {
                error: undefined,
                loaded: false,
                updating: false
            });
        case authenticator_action_1.CANCEL_LOGOUT:
            return state;
        default:
            return state;
    }
}; };
exports.accessTokenState = {
    accessToken: undefined,
    error: undefined,
    loaded: false,
    ttlLeft: 0,
    updating: false,
};
var calculateLeftTtl = function (crDate, ttl) {
    var dateNowInSeconds = new Date().getTime() / 1000;
    var dateCreatedInSeconds = new Date(crDate).getTime() / 1000;
    var lifeTime = dateNowInSeconds - dateCreatedInSeconds;
    return Math.floor(ttl - lifeTime);
};
exports.createValidateAccessTokenReducer = function () { return function (state, action) {
    if (state === void 0) { state = exports.accessTokenState; }
    switch (action.type) {
        case authenticator_action_1.LOADING_VALIDATION:
            return {
                accessToken: undefined,
                error: undefined,
                loaded: false,
                ttlLeft: 0,
                updating: true,
            };
        case authenticator_action_1.SUCCESSFUL_VALIDATION:
            var accessToken = action.payload;
            return accessToken ? {
                accessToken: accessToken,
                error: undefined,
                loaded: true,
                ttlLeft: calculateLeftTtl(accessToken.created, accessToken.ttl),
                updating: false,
            } : exports.accessTokenState;
        case authenticator_action_1.FAILED_VALIDATION:
            return {
                accessToken: undefined,
                error: action.payload,
                loaded: true,
                ttlLeft: 0,
                updating: false,
            };
        case authenticator_action_1.CLEAR_VALIDATION:
            return exports.accessTokenState;
        case authenticator_action_1.CANCEL_VALIDATION:
            return state;
        default:
            return state;
    }
}; };
//# sourceMappingURL=authenticator.reducer.js.map