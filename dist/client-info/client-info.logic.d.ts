import { RequestApiResult, ClientInfo, RequestApiMeta } from '../types';
export declare const clientInfoLogic: import("redux-logic/definitions/utilities").Override<import("redux-logic/definitions/logic").CreateLogic.Config.Base<RequestApiResult<ClientInfo>, import("redux-logic").Action<string, ClientInfo, RequestApiMeta>, string> & import("redux-logic/definitions/logic").CreateLogic.Config.Validate<RequestApiResult<ClientInfo>, import("redux-logic").Action<string, ClientInfo, RequestApiMeta>, {}, undefined> & import("redux-logic/definitions/logic").CreateLogic.Config.Process<RequestApiResult<ClientInfo>, import("redux-logic").Action<string, ClientInfo, RequestApiMeta>, {}, undefined>, {
    name: string;
    type: string;
    cancelType: string;
}> | import("redux-logic/definitions/utilities").Override<import("redux-logic/definitions/logic").CreateLogic.Config.Base<RequestApiResult<ClientInfo>, import("redux-logic").Action<string, ClientInfo, RequestApiMeta>, string> & import("redux-logic/definitions/logic").CreateLogic.Config.Transform<RequestApiResult<ClientInfo>, import("redux-logic").Action<string, ClientInfo, RequestApiMeta>, {}, undefined> & import("redux-logic/definitions/logic").CreateLogic.Config.Process<RequestApiResult<ClientInfo>, import("redux-logic").Action<string, ClientInfo, RequestApiMeta>, {}, undefined>, {
    name: string;
    type: string;
    cancelType: string;
}>;
