"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var lodash_1 = require("lodash");
var client_info_action_1 = require("./client-info.action");
var initialState = {
    error: undefined,
    loaded: false,
    meta: undefined,
    result: undefined,
    updating: false,
};
exports.clientInfoReducer = function (state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case client_info_action_1.LOADING_CLIENT_INFO:
            return lodash_1.assign({}, state, {
                updating: true,
                loaded: false,
            });
        case client_info_action_1.SUCCEEDED_CLIENT_INFO:
            return lodash_1.assign({}, state, {
                error: undefined,
                loaded: true,
                result: action.payload,
                meta: action.meta,
                updating: false,
            });
        case client_info_action_1.FAILED_CLIENT_INFO:
            return lodash_1.assign({}, state, {
                error: action.payload,
                loaded: true,
                meta: action.meta,
                updating: false,
            });
        case client_info_action_1.CANCEL_CLIENT_INFO:
            return lodash_1.assign({}, state, {
                updating: true,
                loaded: false,
            });
        case client_info_action_1.CLEAR_CLIENT_INFO:
            return initialState;
        default:
            return state;
    }
};
//# sourceMappingURL=client-info.reducer.js.map