import { RequestApiResult, ClientInfo, RequestApiError, RequestApiMeta } from '../types';
export declare const clientInfoReducer: (state: RequestApiResult<ClientInfo> | undefined, action: import("flux-standard-action").FluxStandardAction<string, RequestApiError | ClientInfo, RequestApiMeta>) => RequestApiResult<ClientInfo>;
