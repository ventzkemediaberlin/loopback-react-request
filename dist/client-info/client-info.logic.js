"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var redux_logic_1 = require("redux-logic");
var axios_1 = require("axios");
var lodash_1 = require("lodash");
var client_info_action_1 = require("./client-info.action");
;
var utils_1 = require("../utils");
var fetch = axios_1.default;
exports.clientInfoLogic = redux_logic_1.createLogic({
    type: client_info_action_1.LOADING_CLIENT_INFO,
    cancelType: client_info_action_1.CANCEL_CLIENT_INFO,
    latest: true,
    process: function (_a, dispatcher, done) {
        var action = _a.action;
        var dispatch = dispatcher;
        fetch('https://ipapi.co/json/')
            .then(function (result) {
            var data = lodash_1.assign({}, result.data, {
                timestamp: Math.floor(Date.now() / 1000),
            });
            dispatch(client_info_action_1.succeededClientInfo(data, {
                id: 'clientInfo',
                status: result.status,
                statusText: result.statusText,
            }));
        })
            .catch(function (data) {
            var error = utils_1.errorNotFound(data);
            dispatch(client_info_action_1.failedClientInfo(error, {
                id: 'clientInfo'
            }));
        })
            .then(function () { return done(); });
    }
});
//# sourceMappingURL=client-info.logic.js.map