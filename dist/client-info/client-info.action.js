"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("../utils");
exports.LOADING_CLIENT_INFO = utils_1.createRequestActionType('LOADING', 'client-info');
exports.SUCCEEDED_CLIENT_INFO = utils_1.createRequestActionType('SUCCEEDED', 'client-info');
exports.FAILED_CLIENT_INFO = utils_1.createRequestActionType('FAILED', 'client-info');
exports.CANCEL_CLIENT_INFO = utils_1.createRequestActionType('CANCEL', 'client-info');
exports.CLEAR_CLIENT_INFO = utils_1.createRequestActionType('CLEAR', 'client-info');
/**
 * Action to load client info
 * @returns FSA
 */
exports.loadClientInfo = function () { return ({
    type: exports.LOADING_CLIENT_INFO
}); };
/**
 * Action to load client info
 * @param payload
 * @returns FSA
 */
exports.succeededClientInfo = function (payload, meta) { return ({
    type: exports.SUCCEEDED_CLIENT_INFO,
    payload: payload,
    meta: meta,
}); };
/**
 * Action to load client info
 * @param payload
 * @returns FSA
 */
exports.failedClientInfo = function (payload, meta) { return ({
    type: exports.FAILED_CLIENT_INFO,
    payload: payload,
    meta: meta,
}); };
/**
 * Action to load client info
 * @param payload
 * @returns FSA
 */
exports.cancelClientInfo = function () { return ({
    type: exports.CANCEL_CLIENT_INFO,
}); };
/**
 * Action to load client info
 * @param payload
 * @returns FSA
 */
exports.clearClientInfo = function () { return ({
    type: exports.CLEAR_CLIENT_INFO,
}); };
//# sourceMappingURL=client-info.action.js.map