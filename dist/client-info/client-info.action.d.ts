import { ClientInfo, RequestApiMeta, RequestApiError } from '../types';
export declare const LOADING_CLIENT_INFO: string;
export declare const SUCCEEDED_CLIENT_INFO: string;
export declare const FAILED_CLIENT_INFO: string;
export declare const CANCEL_CLIENT_INFO: string;
export declare const CLEAR_CLIENT_INFO: string;
/**
 * Action to load client info
 * @returns FSA
 */
export declare const loadClientInfo: () => import("flux-standard-action").FluxStandardAction<string, undefined, undefined>;
/**
 * Action to load client info
 * @param payload
 * @returns FSA
 */
export declare const succeededClientInfo: (payload: ClientInfo, meta: RequestApiMeta) => import("flux-standard-action").FluxStandardAction<string, ClientInfo, RequestApiMeta>;
/**
 * Action to load client info
 * @param payload
 * @returns FSA
 */
export declare const failedClientInfo: (payload: RequestApiError, meta: RequestApiMeta) => import("flux-standard-action").FluxStandardAction<string, RequestApiError, RequestApiMeta>;
/**
 * Action to load client info
 * @param payload
 * @returns FSA
 */
export declare const cancelClientInfo: () => import("flux-standard-action").FluxStandardAction<string, undefined, undefined>;
/**
 * Action to load client info
 * @param payload
 * @returns FSA
 */
export declare const clearClientInfo: () => import("flux-standard-action").FluxStandardAction<string, undefined, undefined>;
