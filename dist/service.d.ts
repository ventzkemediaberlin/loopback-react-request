import { AxiosRequestConfig } from 'axios';
import { AppSettings, HttpServiceProps } from './types';
/**
 * Class representing the http service, it get called when dispatching the `requestAPIActions.load()`
 * or `requestAPIActions.update()` action type.
 *
 * ```javascript
 * var example = 'test
 * ```
 *
 * @class HttpService
 * @param {Object} props Properties to set up the HttpService Class
 * @param {Object} appSettings Properties that are neccessary to get this package to run.
 * @property {Object} appSettings Object that holds the app settings.
 * @property {Object} props Object that holds the properties that are neccessary to get this package to run.
 * @property {Object} accessToken Object that holds the the Loopback access token.
 * @property {String} API_URL Url to the API.
 * @property {Object} headers Object that holds the http headers.
 * @property {Object} options Object that holds the AxiosRequestConfig
 * @property {String} queryParams Query parameter.
 * @property {String} queryType Describes the Query type, Default is `filter`
 * @property {String} url Url to Rest Api
 */
export declare class HttpService<ApiResult> {
    private appSettings;
    private props;
    constructor(props: HttpServiceProps, appSettings: AppSettings);
    readonly accessToken: import("./types").AccessToken | undefined;
    readonly API_URL: string;
    readonly headers: any;
    readonly options: AxiosRequestConfig;
    readonly queryParams: string;
    readonly queryType: string | undefined;
    readonly url: string;
    fetch: () => any;
}
