"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var lodash_1 = require("lodash");
var request_1 = require("./request");
var config_1 = require("./config");
var authenticator_1 = require("./authenticator");
var client_info_1 = require("./client-info");
var defaultAppSettings = {
    cookieName: 'authToken',
    config: config_1.restApiConfig,
};
/**
 * Class representing the the redux state and logic,
 * it creates an array with all informations by a key
 * @class
 * @param {Array} identifiers Array of request Id's
 * @param {Object} appSettings Additional app settings.
 * @property {Object} appSettings Holds the additional app settings.
 * @property {Array} combinedLogic Holds all created redux logics.
 * @property {Object} combinedStates Holds all created redux states.
 * @property {Object} combinedReducerWithLogic Holds all redux logic, state and Id's.
 * @property {Array} identifiers Array of existing request Id's.
 */
var RequestStore = /** @class */ (function () {
    function RequestStore(identifiers, appSettings) {
        this.reducerIds = {
            public: [],
            secure: [],
        };
        this.appSettings = appSettings ? appSettings : defaultAppSettings;
        this.reducerIds = identifiers;
    }
    Object.defineProperty(RequestStore.prototype, "combinedLogic", {
        get: function () {
            var combinedLogic = [];
            this.combinedReducerWithLogic.map(function (store) { return store.logic && combinedLogic.push(store.logic); });
            return combinedLogic;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestStore.prototype, "combinedStates", {
        get: function () {
            var combinedStates = {};
            this.combinedReducerWithLogic.map(function (store) {
                var _a;
                return store.reducer && lodash_1.assign(combinedStates, (_a = {},
                    _a[store.id] = store.reducer,
                    _a));
            });
            return combinedStates;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestStore.prototype, "combinedReducerWithLogic", {
        get: function () {
            return this.combineReducerWithLogic();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestStore.prototype, "defaultReducerWithLogic", {
        get: function () {
            var authLogics = authenticator_1.creatAuthenticationLogic(this.reducerIds, this.appSettings);
            return [{
                    id: 'authenticator',
                    reducer: authenticator_1.createAuthenticatorReducer(this.appSettings),
                }, {
                    id: 'authentification',
                    logic: authLogics.fetchAuthentification(),
                    reducer: authenticator_1.createAuthentificationReducer(this.appSettings),
                }, {
                    id: 'clientInfo',
                    logic: client_info_1.clientInfoLogic,
                    reducer: client_info_1.clientInfoReducer,
                }, {
                    id: 'logout',
                    logic: authLogics.fetchLogout(),
                    reducer: authenticator_1.createLogoutReducer(),
                }, {
                    id: 'validateToken',
                    logic: authLogics.validateAccessToken(),
                    reducer: authenticator_1.createValidateAccessTokenReducer(),
                }];
        },
        enumerable: true,
        configurable: true
    });
    RequestStore.prototype.combineReducerWithLogic = function () {
        var _this = this;
        var combined = [];
        this.identifiers.map(function (identifier) { return combined.push({
            id: identifier,
            logic: request_1.createRequestLogic(identifier, _this.appSettings, _this.reducerIds.secure),
            reducer: request_1.createRequestReducer(identifier),
        }); });
        return lodash_1.concat([], combined, this.defaultReducerWithLogic);
    };
    Object.defineProperty(RequestStore.prototype, "identifiers", {
        get: function () {
            return lodash_1.concat([], this.reducerIds.public, this.reducerIds.secure);
        },
        enumerable: true,
        configurable: true
    });
    return RequestStore;
}());
exports.RequestStore = RequestStore;
//# sourceMappingURL=state.js.map