"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Cookie = require("js-cookie");
var lodash_1 = require("lodash");
var request_1 = require("./request");
/**
 * Function to create the the Redux actions type that will be dispatched by each state when
 * calling an action.
 * @param {String} defaultType
 * @param {String} extendedType
 * @returns {String} Redux action type
 */
function createRequestActionType(defaultType, extendedType) {
    return "@ReduxHttpRequest/" + defaultType + "_" + extendedType.toUpperCase();
}
exports.createRequestActionType = createRequestActionType;
/**
 * Get access token from cookie
 * @param {String} ACCESSTOKEN_NAME
 * @returns {Object} AccessToken
 */
function getAccessTokenFromCookie(ACCESSTOKEN_NAME) {
    var accessToken = Cookie.getJSON(ACCESSTOKEN_NAME);
    if (accessToken && accessToken.id && accessToken.id.length) {
        return accessToken;
    }
    return undefined;
}
exports.getAccessTokenFromCookie = getAccessTokenFromCookie;
function updateResultFromRow(state, payload) {
    if (payload) {
        var prevResult = state && state.result;
        if (prevResult) {
            var rows = lodash_1.concat(prevResult.rows || [], payload.rows || []);
            return {
                start: prevResult.start,
                end: payload.end,
                rowCount: rows && rows.length,
                rows: rows && rows.length > 0 ? rows : [],
                totalRowCount: payload.totalRowCount,
            };
        }
        else {
            return payload;
        }
    }
    return;
}
exports.updateResultFromRow = updateResultFromRow;
function updateResultFromSearch(state, payload) {
    if (payload) {
        var prevResult_1 = state && state.result;
        if (prevResult_1) {
            var keys = Object.keys(prevResult_1);
            var newResult_1 = {};
            keys.map(function (key) {
                var resultFromState = prevResult_1[key];
                var resultFromPayload = payload[key];
                if (resultFromState) {
                    var rows = lodash_1.concat(resultFromState.rows || [], resultFromPayload.rows || []);
                    newResult_1[key] = {
                        start: resultFromState.start,
                        end: resultFromPayload.end,
                        rowCount: rows && rows.length,
                        rows: rows && rows.length > 0 ? rows : [],
                        totalRowCount: resultFromPayload.totalRowCount,
                    };
                }
                else {
                    newResult_1[key] = resultFromPayload;
                }
            });
            return newResult_1;
        }
        else {
            return payload;
        }
    }
    return;
}
exports.updateResultFromSearch = updateResultFromSearch;
exports.errorNotFound = function (error) {
    var responseError = error.response && error.response.data && error.response.data.error ?
        error.response.data.error :
        error.response && error.response.data;
    return responseError || {
        code: 'NOT_FOUND',
        message: 'Could not connect to Api',
        name: 'Not found',
        statusCode: 404,
    };
};
/**
 * Clears a list of states stored in global state by given keys.
 * @param {Array} keys List of keys
 * @param {ClearStateProps} props Object that holds all information for clearing states
 * @returns Function
 */
exports.clearStateByListOfKeys = function (keys, props) { return keys.forEach(function (key) { return exports.clearRequestStates({
    dispatch: props.dispatch,
    key: key,
    state: props.state && props.state[key] ? props.state[key] : undefined,
}); }); };
/**
 * Clears one state bay given key
 * @param {ClearStateProps} props Object that holds all information for clearing states
 */
exports.clearRequestStates = function (props) {
    return props.state && props.key && props.state[props.key] && props.state[props.key].result &&
        props.key && props.dispatch(request_1.requestAPIActions.loadCleared(props.key));
};
//# sourceMappingURL=utils.js.map