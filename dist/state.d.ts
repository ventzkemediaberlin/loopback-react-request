import { CombineReducerWithLogic, CreateRequestReducer, AppSettings, ReducerIds } from './types';
import { Logic } from 'redux-logic';
/**
 * Class representing the the redux state and logic,
 * it creates an array with all informations by a key
 * @class
 * @param {Array} identifiers Array of request Id's
 * @param {Object} appSettings Additional app settings.
 * @property {Object} appSettings Holds the additional app settings.
 * @property {Array} combinedLogic Holds all created redux logics.
 * @property {Object} combinedStates Holds all created redux states.
 * @property {Object} combinedReducerWithLogic Holds all redux logic, state and Id's.
 * @property {Array} identifiers Array of existing request Id's.
 */
export declare class RequestStore<ApiResult> {
    appSettings: AppSettings;
    readonly combinedLogic: Logic<any>[];
    readonly combinedStates: CreateRequestReducer<ApiResult> | {};
    readonly combinedReducerWithLogic: CombineReducerWithLogic<ApiResult>[];
    readonly defaultReducerWithLogic: CombineReducerWithLogic<{}>[];
    combineReducerWithLogic<R>(): CombineReducerWithLogic<R>[];
    readonly identifiers: string[];
    reducerIds: ReducerIds;
    constructor(identifiers: ReducerIds, appSettings?: AppSettings);
}
