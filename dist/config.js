"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var headers = {};
exports.restApiConfig = {
    port: 3300,
    protokol: 'http',
    url: 'localhost',
    path: '/api'
};
exports.default = {
    headers: headers,
    restApiConfig: exports.restApiConfig,
};
//# sourceMappingURL=config.js.map