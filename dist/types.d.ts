import { AxiosRequestConfig, AxiosError, AxiosResponse } from 'axios';
import { Logic } from 'redux-logic';
import { FSA } from 'flux-standard-action';
/**
 * Types for HTTP Service
 */
declare type GET = 'GET';
declare type PATCH = 'PATCH';
declare type POST = 'POST';
declare type PUT = 'PUT';
export declare type HttpRequestMethod = GET | PATCH | POST | PUT;
export interface HttpRequestHeaders {
    [key: string]: string | number;
}
export interface FetchOptions {
}
export interface RequestApiErrorDetails {
    codes: {
        [key: string]: string | string[];
    };
    context: string;
    messages: {
        [key: string]: string | string[];
    };
}
export interface RequestApiError {
    details?: RequestApiErrorDetails;
    code: string;
    message: string;
    name: string;
    stack?: string;
    statusCode: number;
}
export declare type QueryFilterTypes = string | number | boolean | string[] | number[] | boolean[] | RegExp | RegExp[];
export interface QueryParams {
    [key: string]: QueryFilterTypes | QueryParams;
}
export declare type QueryFilterWhereTypes = 'and' | 'or' | 'gt' | 'gte' | 'lt' | 'lte' | 'between' | 'inq' | 'nin' | 'near' | 'neq' | 'like' | 'nlike' | 'ilike' | 'nilike' | 'reqexp';
export interface QueryWhereFilter {
    [QueryFilterWhereTypes: string]: QueryParams | QueryParams[] | QueryFilterTypes | QueryFilterTypes[];
}
export interface QueryFilterParams {
    addressId?: number;
    fields?: QueryFilterTypes;
    include?: QueryFilterTypes;
    lang?: number;
    limit?: QueryFilterTypes;
    order?: QueryFilterTypes;
    projectId?: number;
    skip?: QueryFilterTypes;
    sword?: string | QueryWhereFilter | QueryWhereFilter[];
    where?: QueryWhereFilter | QueryWhereFilter[];
}
export interface RequestApiHttpPayload {
    options?: AxiosRequestConfig;
    queryParams?: QueryFilterParams;
    queryType?: 'search' | 'filter' | false;
    url?: string;
    isPager?: boolean;
    isSearch?: boolean;
    update?: boolean;
}
export interface HttpServiceProps extends RequestApiHttpPayload {
}
export declare type RequestApiId = string;
export declare type RequestApiType = HttpRequestMethod;
export declare type RequestApiPayload = {} | RequestApiError | RequestApiHttpPayload;
export interface RequestApiMeta {
    id: string;
    options?: RequestApiHttpPayload;
    status?: number;
    statusText?: string;
    update?: boolean;
    isPager?: boolean;
    isSearch?: boolean;
}
export interface RequestApiResult<R> {
    error?: RequestApiError;
    loaded: boolean;
    meta?: RequestApiMeta;
    result?: R;
    updating: boolean;
}
export interface RequestAPIRespond<M> {
    end: number;
    rowCount: number;
    rows?: M | M[] | null;
    start: number;
    totalRowCount: number;
}
declare type Protocol = 'https' | 'http';
export interface ConfigRestApi {
    path: string;
    port?: number;
    protokol: Protocol;
    url: string;
}
export interface AppSettings {
    cookieName: string;
    config: ConfigRestApi;
    userRemote?: string;
}
export interface RequestAPITypes {
    LOAD_CANCELD: 'LOAD_CANCELD';
    LOAD_CLEARED: 'LOAD_CLEARED';
    LOAD_FAILED: 'LOAD_FAILED';
    LOAD_REQUEST: 'LOAD_REQUEST';
    LOAD_SUCCEEDED: 'LOAD_SUCCEEDED';
    LOAD_UPDATE: 'LOAD_UPDATE';
}
export interface CombineReducerWithLogic<R> {
    id: string;
    logic?: any;
    reducer?: CreateRequestReducer<R> | Function;
}
export declare type RequestReducer = <ApiResult>(state: RequestApiResult<ApiResult> | undefined, action: FSA<string, ApiResult, RequestApiMeta>) => RequestApiResult<ApiResult>;
export declare type CreateLogic = (config: Logic<any>) => Logic<any>;
export declare type CreateRequestLogic = <ApiResult>(requestId: string, appSettings: AppSettings) => Logic<any>;
export declare type CreateRequestReducer<ApiResult> = (state: RequestApiResult<ApiResult> | undefined, action: FSA<string, RequestApiResult<ApiResult>, RequestApiMeta>) => RequestApiResult<ApiResult>;
export interface AccessToken {
    created: Date;
    id: string;
    scope?: null | object;
    ttl: number;
    userId: number;
}
export interface RegistrationApiResponse {
    result: {};
    created: boolean;
    emailSend: boolean;
}
export interface AxiosErrorCustom extends AxiosError {
    response?: AxiosResponse<{
        error: RequestApiError;
    }>;
}
export interface AuthentificationAction {
    type: string;
    payload: AuthentificationData | AccessToken | RequestApiError | undefined;
}
export interface LogoutAction {
    type: string;
    payload?: AccessToken | RequestApiError;
}
export interface LogoutDispatch {
    loadLogout(payload?: AccessToken): LogoutAction;
    succeededLogout(): LogoutAction;
    failedLogout(payload?: RequestApiError): LogoutAction;
    clearLogout(): LogoutAction;
}
export interface AuthentificationData {
    email: string;
    password: string;
}
export interface IsAuthentificatedState {
    accessToken?: AccessToken;
    isAuthentificated: boolean;
}
export interface AuthentificationState {
    accessToken?: AccessToken;
    error?: RequestApiError;
    loaded: boolean;
    updating: boolean;
}
export interface ValidateTokenState extends AuthentificationState {
    ttlLeft: number;
}
export interface LogoutState {
    error?: RequestApiError;
    loaded: boolean;
    updating: boolean;
}
export interface ReducerIds {
    public: string[];
    secure: string[];
}
export interface ClientInfo {
    ip: number;
    city: string;
    region: string;
    region_code: string;
    country: string;
    country_name: string;
    continent_code: string;
    in_eu: boolean;
    postal: number;
    latitude: number;
    longitude: number;
    timezone: string;
    timestamp: number;
    utc_offset: number;
    country_calling_code: number;
    currency: string;
    languages: string;
    asn: string;
    org: string;
}
export {};
