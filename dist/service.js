"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var lodash_1 = require("lodash");
var qs_1 = require("qs");
var utils_1 = require("./utils");
/**
 * Class representing the http service, it get called when dispatching the `requestAPIActions.load()`
 * or `requestAPIActions.update()` action type.
 *
 * ```javascript
 * var example = 'test
 * ```
 *
 * @class HttpService
 * @param {Object} props Properties to set up the HttpService Class
 * @param {Object} appSettings Properties that are neccessary to get this package to run.
 * @property {Object} appSettings Object that holds the app settings.
 * @property {Object} props Object that holds the properties that are neccessary to get this package to run.
 * @property {Object} accessToken Object that holds the the Loopback access token.
 * @property {String} API_URL Url to the API.
 * @property {Object} headers Object that holds the http headers.
 * @property {Object} options Object that holds the AxiosRequestConfig
 * @property {String} queryParams Query parameter.
 * @property {String} queryType Describes the Query type, Default is `filter`
 * @property {String} url Url to Rest Api
 */
var HttpService = /** @class */ (function () {
    function HttpService(props, appSettings) {
        var _this = this;
        this.fetch = function () {
            var options = _this.options;
            if (_this.accessToken) {
                options.headers = lodash_1.assign(_this.options.headers, {
                    'x-access-token': _this.accessToken.id,
                });
            }
            var fetch = axios_1.default;
            return fetch(_this.url, options);
        };
        this.appSettings = appSettings;
        this.props = props;
    }
    Object.defineProperty(HttpService.prototype, "accessToken", {
        get: function () {
            return utils_1.getAccessTokenFromCookie(this.appSettings.cookieName);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HttpService.prototype, "API_URL", {
        get: function () {
            var config = this.appSettings.config;
            var port = config.port ? ":" + config.port : '';
            var path = config.path ? config.path : '';
            return config.protokol + "://" + config.url + port + path;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HttpService.prototype, "headers", {
        get: function () {
            return lodash_1.assign({}, this.options.headers, {
                'Content-Type': 'application/json; charset=utf-8',
            });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HttpService.prototype, "options", {
        get: function () {
            return lodash_1.assign({}, this.props.options);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HttpService.prototype, "queryParams", {
        get: function () {
            var _a;
            if (this.queryType && this.queryType.length > 0) {
                return this.props.queryParams ? "?" + qs_1.stringify((_a = {}, _a[this.queryType] = this.props.queryParams, _a)) : '';
            }
            else {
                return this.props.queryParams ? "?" + qs_1.stringify(this.props.queryParams) : '';
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HttpService.prototype, "queryType", {
        get: function () {
            return this.props.queryType !== false ? this.props.queryType && this.props.queryType.length > 0 ? this.props.queryType : 'filter' : undefined;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HttpService.prototype, "url", {
        get: function () {
            return "" + this.API_URL + this.props.url + this.queryParams;
        },
        enumerable: true,
        configurable: true
    });
    return HttpService;
}());
exports.HttpService = HttpService;
//# sourceMappingURL=service.js.map