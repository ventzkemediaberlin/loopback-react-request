import { AccessToken, RequestAPIRespond, RequestApiResult, AxiosErrorCustom } from './types';
import { AxiosError } from 'axios';
/**
 * Function to create the the Redux actions type that will be dispatched by each state when
 * calling an action.
 * @param {String} defaultType
 * @param {String} extendedType
 * @returns {String} Redux action type
 */
export declare function createRequestActionType(defaultType: string, extendedType: string): string;
/**
 * Get access token from cookie
 * @param {String} ACCESSTOKEN_NAME
 * @returns {Object} AccessToken
 */
export declare function getAccessTokenFromCookie(ACCESSTOKEN_NAME: string): AccessToken | undefined;
export declare function updateResultFromRow<M = {}>(state: RequestApiResult<RequestAPIRespond<M>>, payload?: RequestAPIRespond<M>): RequestAPIRespond<M> | undefined;
export declare function updateResultFromSearch<M = {}>(state: RequestApiResult<{
    [key: string]: RequestAPIRespond<M>;
}>, payload?: {
    [key: string]: RequestAPIRespond<M>;
}): {
    [key: string]: RequestAPIRespond<M>;
} | undefined;
export declare const errorNotFound: <R = {}>(error: AxiosError | AxiosErrorCustom) => import("./types").RequestApiError | undefined;
export interface ClearStateProps<M> {
    dispatch: Function;
    key?: string;
    state?: RequestApiResult<M>[];
}
/**
 * Clears a list of states stored in global state by given keys.
 * @param {Array} keys List of keys
 * @param {ClearStateProps} props Object that holds all information for clearing states
 * @returns Function
 */
export declare const clearStateByListOfKeys: <M>(keys: string[], props: ClearStateProps<M>) => void;
/**
 * Clears one state bay given key
 * @param {ClearStateProps} props Object that holds all information for clearing states
 */
export declare const clearRequestStates: <M>(props: ClearStateProps<M>) => void;
